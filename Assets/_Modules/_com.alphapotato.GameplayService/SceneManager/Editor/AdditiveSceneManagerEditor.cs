﻿namespace com.alphapotato.GameplayService {

    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor.SceneManagement;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor (typeof (AdditiveSceneManager), true)]
    public class AdditiveSceneManagerEditor : Editor {

        private AdditiveSceneManager Reference;

        private void OnEnable () {

            Reference = (AdditiveSceneManager) target;
        }

        public override void OnInspectorGUI () {

            serializedObject.Update ();

            base.OnInspectorGUI ();

            if (Reference.listOfAdditiveScene != null) {

                int t_NumberOfAdditiveScene = Reference.listOfAdditiveScene.Count;
                for (int i = 0; i < t_NumberOfAdditiveScene; i++) {

                    Reference.listOfAdditiveScene[i].showSceneAsset = EditorGUILayout.Foldout (
                        Reference.listOfAdditiveScene[i].showSceneAsset,
                        "Scene (" + (i + 1) + ") : " + (Reference.listOfAdditiveScene[i].scenePath == "" ? "NotAssigned!" : Reference.GetSceneName (i)),
                        true
                    );

                    if (Reference.listOfAdditiveScene[i].showSceneAsset) {

                        if (!PanelForEditAndRemovingScene (i)) {
                            break;
                        }

                    }
                }
            }

            EditorGUILayout.Space ();
            PanelForAddingScene ();

            serializedObject.ApplyModifiedProperties ();
        }

        #region Configuretion

        private bool IsSceneAlreadyInBuild (string t_ScenePath) {

            int t_NumberOfSceneInBuild = EditorBuildSettings.scenes.Length;
            for (int i = 0; i < t_NumberOfSceneInBuild; i++) {
                if (EditorBuildSettings.scenes[i].path == t_ScenePath) {
                    return true;
                }
            }
            return false;
        }

        private void RemoveSceneFromBuild (string t_ScenePath) {

            List<EditorBuildSettingsScene> t_TempBuildSettingsScene = EditorBuildSettings.scenes.ToList ();
            int t_NumberOfCurrentSceneInTheBuild = t_TempBuildSettingsScene.Count;
            for (int j = 0; j < t_NumberOfCurrentSceneInTheBuild; j++) {
                if (t_TempBuildSettingsScene[j].path == t_ScenePath) {
                    t_TempBuildSettingsScene.RemoveAt (j);
                    t_TempBuildSettingsScene.TrimExcess ();
                    break;
                }
            }
            EditorBuildSettings.scenes = t_TempBuildSettingsScene.ToArray ();
        }

        #endregion

        #region CustomInspector

        private bool PanelForEditAndRemovingScene (int t_SceneIndex) {

            EditorGUI.indentLevel += 1;

            //Panel : Adding & RemovingScene
            EditorGUILayout.BeginHorizontal (); {
                SceneAsset t_NewSceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset> (Reference.listOfAdditiveScene[t_SceneIndex].scenePath);
                EditorGUI.BeginChangeCheck ();
                Object t_NewSceneObject = EditorGUILayout.ObjectField (
                    "SceneAsset",
                    t_NewSceneAsset,
                    typeof (SceneAsset), false) as SceneAsset;
                if (EditorGUI.EndChangeCheck ()) {

                    string t_NewSceneDataPath = AssetDatabase.GetAssetPath (t_NewSceneObject);
                    SerializedProperty t_NewScenePathProperty = serializedObject.FindProperty ("listOfAdditiveScene").GetArrayElementAtIndex (t_SceneIndex).FindPropertyRelative ("scenePath");
                    SerializedProperty t_NewSceneNameProperty = serializedObject.FindProperty ("listOfAdditiveScene").GetArrayElementAtIndex (t_SceneIndex).FindPropertyRelative ("sceneName");
                    t_NewScenePathProperty.stringValue = t_NewSceneDataPath;

                    string[] t_SplitByDash = t_NewSceneDataPath.Split ('/');
                    string[] t_SplitByDot = t_SplitByDash[t_SplitByDash.Length - 1].Split ('.');
                    t_NewSceneNameProperty.stringValue = t_SplitByDot[0];

                    //Debug.Log(EditorSceneManager.GetSceneByName(t_SplitByDot[0]).GetRootGameObjects().Length);
                }

                if (GUILayout.Button ("Remove")) {

                    int t_NumberOfAvailableScene = Reference.listOfAdditiveScene.Count;
                    for(int selectedScene = 0 ; selectedScene < t_NumberOfAvailableScene; selectedScene++){

                        if(selectedScene != t_SceneIndex){

                            Reference.listOfAdditiveScene[selectedScene].sceneMapForEnable.RemoveAt(t_SceneIndex);
                        }
                    }

                    RemoveSceneFromBuild (Reference.listOfAdditiveScene[t_SceneIndex].scenePath);
                    Reference.listOfAdditiveScene.RemoveAt (t_SceneIndex);
                    return false;
                }
            }
            EditorGUILayout.EndHorizontal ();

            PanelForSceneSettingsOfScene(t_SceneIndex);

            if (!EditorApplication.isPlaying)
                PanelForBuildSettingsOfScene (t_SceneIndex);

            EditorGUI.indentLevel -= 1;

            return true;
        }

        private void PanelForSceneSettingsOfScene (int t_SceneIndex) {

            Reference.listOfAdditiveScene[t_SceneIndex].showSceneSettingsForScene = EditorGUILayout.Foldout (
                Reference.listOfAdditiveScene[t_SceneIndex].showSceneSettingsForScene,
                "SceneSettings",
                true);

            if (Reference.listOfAdditiveScene[t_SceneIndex].showSceneSettingsForScene) {

                EditorGUILayout.PropertyField(serializedObject.FindProperty ("listOfAdditiveScene").GetArrayElementAtIndex (t_SceneIndex).FindPropertyRelative ("sceneTypeForLoading"));

                switch (Reference.listOfAdditiveScene[t_SceneIndex].sceneTypeForLoading) {
                    case AdditiveSceneManager.SceneTypeForLoading.Default:
                        break;
                    case AdditiveSceneManager.SceneTypeForLoading.RemoveAllOtherScene:
                        break;
                    case AdditiveSceneManager.SceneTypeForLoading.Custom:
                        int t_NumberOfScene = Reference.listOfAdditiveScene.Count;
                        if (t_NumberOfScene == 1) {
                            EditorGUILayout.HelpBox ("In order to use custom loading method, please add more than 1  scene", MessageType.Warning);
                        } else {

                            EditorGUI.indentLevel += 1;

                            for (int i = 0; i < t_NumberOfScene; i++) {
                                if (t_SceneIndex != i) {
                                    Reference.listOfAdditiveScene[t_SceneIndex].sceneMapForEnable[i] = EditorGUILayout.Toggle (
                                        "Scene (" + (i + 1) + ") : " + Reference.GetSceneName (i),
                                        Reference.listOfAdditiveScene[t_SceneIndex].sceneMapForEnable[i]);
                                }
                            }

                            EditorGUI.indentLevel -= 1;
                        }

                        break;
                }

                if (IsSceneAlreadyInBuild (Reference.listOfAdditiveScene[t_SceneIndex].scenePath)) {

                    if (!Reference.IsSceneLoaded (t_SceneIndex)) {

                        if (GUILayout.Button ("LoadScene")) {
                            Reference.LoadScene (t_SceneIndex);
                        }
                    } else {

                        if (GUILayout.Button ("RemoveScene")) {
                            Reference.ProcessOnUnloadScene (t_SceneIndex);
                        }
                    }

                } else {
                    EditorGUILayout.HelpBox ("In order to load scene, please add it to the build", MessageType.Warning);
                }
            }
        }

        private void PanelForBuildSettingsOfScene (int t_SceneIndex) {

            Reference.listOfAdditiveScene[t_SceneIndex].showBuildSettingsForScene = EditorGUILayout.Foldout (
                Reference.listOfAdditiveScene[t_SceneIndex].showBuildSettingsForScene,
                "BuildSettings",
                true);

            if (Reference.listOfAdditiveScene[t_SceneIndex].showBuildSettingsForScene) {

                EditorGUI.indentLevel += 1;

                string t_ScenePath = Reference.listOfAdditiveScene[t_SceneIndex].scenePath;
                bool t_IsSceneAlreadyInBuild = IsSceneAlreadyInBuild (t_ScenePath);

                if (t_IsSceneAlreadyInBuild) {

                    EditorGUILayout.BeginHorizontal (); {

                        EditorGUI.BeginChangeCheck ();
                        Reference.listOfAdditiveScene[t_SceneIndex].isSceneEnabledInBuild = EditorGUILayout.Toggle (
                            "Is Enabled",
                            Reference.listOfAdditiveScene[t_SceneIndex].isSceneEnabledInBuild);
                        if (EditorGUI.EndChangeCheck ()) {
                            Debug.Log (Reference.listOfAdditiveScene[t_SceneIndex].isSceneEnabledInBuild);

                            int t_NumberOfAvailableScene = Reference.listOfAdditiveScene.Count;
                            for (int i = 0; i < t_NumberOfAvailableScene; i++) {

                                if (EditorBuildSettings.scenes[i].path == t_ScenePath) {

                                    EditorBuildSettingsScene[] t_TempBuildSettingsScene = EditorBuildSettings.scenes;
                                    t_TempBuildSettingsScene[i].enabled = Reference.listOfAdditiveScene[t_SceneIndex].isSceneEnabledInBuild;
                                    EditorBuildSettings.scenes = t_TempBuildSettingsScene;

                                    break;
                                }
                            }

                        }

                        if (GUILayout.Button ("Remove From Build")) {
                            RemoveSceneFromBuild (t_ScenePath);
                        }
                    }
                    EditorGUILayout.EndHorizontal ();

                } else {
                    if (GUILayout.Button ("Add To Build")) {

                        EditorBuildSettingsScene t_NewBuildScene = new EditorBuildSettingsScene (t_ScenePath, true);
                        List<EditorBuildSettingsScene> t_TempBuildSettingsScene = EditorBuildSettings.scenes.ToList ();
                        t_TempBuildSettingsScene.Add (t_NewBuildScene);
                        EditorBuildSettings.scenes = t_TempBuildSettingsScene.ToArray ();

                        Reference.listOfAdditiveScene[t_SceneIndex].isSceneEnabledInBuild = true;
                    }
                }

                EditorGUI.indentLevel -= 1;
            }
        }

        private void PanelForAddingScene () {

            if (GUILayout.Button ("Add Scene")) {

                Reference.CreateListOfAddtiveSceneInfo ();
                Reference.CreateAnEmptyAdditiveSceneInfo ();

            }
        }

        #endregion
    }
}