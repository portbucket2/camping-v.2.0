﻿namespace com.faithstudio.Camera
{
    using UnityEngine;

    using System.Collections.Generic;

    using com.faithstudio.Math;
    using com.faithstudio.Gameplay;

    public class TouchCamera : MonoBehaviour
    {
        #region Public Variables

        public Transform verticalMark;
        public Transform horizontalMark;

        [Space(5.0f)]
        public Transform cameraTransformReference;
        public Transform focusPoint;
        [Range(0f, 1f)]
        public float forwardVelocityOfCamera;

        [Space(5.0f)]
        public CameraFocus cameraFocusReference;
        public LerpTouchController lerpTouchControllerReference;

        #endregion

        #region Private Variables

        private bool m_IsTouchCameraControllerRunning;

        private List<Transform> m_CameraFocusingObject;

        private Vector3 m_TargetedPosition;
        private Vector3 m_ModifiedPosition;

        #endregion

        #region Mono Behaviour

        private void LateUpdate()
        {
            if (m_IsTouchCameraControllerRunning)
            {

                m_TargetedPosition = new Vector3(
                        horizontalMark.position.x,
                        verticalMark.position.y,
                        (horizontalMark.position.z + verticalMark.position.z) / 2.0f
                    );
                m_ModifiedPosition = Vector3.Lerp(
                        cameraTransformReference.position,
                        m_TargetedPosition,
                        forwardVelocityOfCamera
                    );
                cameraTransformReference.position = m_ModifiedPosition;

            }
        }

        #endregion

        #region Configuretion

        private void OnTouchDown(Vector3 t_TouchPosition)
        {

            cameraFocusReference.FocusCamera(
                    m_CameraFocusingObject,
                    Vector3.zero,
                    0.1f,
                    0.75f,
                    0.825f
                );
        }

        private void OnTouchUp(Vector3 t_TouchPosition)
        {
            cameraFocusReference.UnfocusCamera();
        }

        #endregion

        #region Public Callback

        public void PreProcess(List<Transform> t_CameraFocusingObject = null)
        {

            if (t_CameraFocusingObject == null)
                m_CameraFocusingObject = new List<Transform>() { focusPoint };
            else
                m_CameraFocusingObject = t_CameraFocusingObject;


            m_IsTouchCameraControllerRunning = true;
            lerpTouchControllerReference.EnableLerpTouchController(true);
            GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
            GlobalTouchController.Instance.OnTouchUp += OnTouchUp;
        }

        public void PostProcess()
        {

            m_IsTouchCameraControllerRunning = false;
            lerpTouchControllerReference.DisableLerpTouchController();
            GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
            GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;
        }

        #endregion

    }
}

