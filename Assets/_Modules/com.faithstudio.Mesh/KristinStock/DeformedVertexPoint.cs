﻿
namespace com.faithstudio.Mesh
{
    using UnityEngine;

    public class DeformedVertexPoint
    {

        public int m_VertexIndex;
        public Vector2 m_VertextGridPosition;
        public Vector3 m_InitialVertexPosition;
        public Vector3 m_CurrentVertexPosition;

        private bool m_IsDeformationAlowed;

        public DeformedVertexPoint(int t_VertexIndex, Vector2 t_VertexGridPosition, Vector3 t_InitialVertexPosition)
        {

            m_IsDeformationAlowed = true;

            m_VertexIndex = t_VertexIndex;
            m_VertextGridPosition = t_VertexGridPosition;
            m_InitialVertexPosition = t_InitialVertexPosition;
            m_CurrentVertexPosition = t_InitialVertexPosition;
        }

        public void EnableDeformation()
        {
            m_IsDeformationAlowed = true;
        }

        public void DisableDeformation() {

            m_IsDeformationAlowed = false;
        }

        public bool IsDeformationAllowed() {

            return m_IsDeformationAlowed;
        }

        public Vector3 GetCurrentVertexPosition()
        {

            return m_CurrentVertexPosition;
        }

        public Vector3 GetCurrentDisplacement()
        {

            return m_CurrentVertexPosition - m_InitialVertexPosition;
        }

        public Vector3 GetDeformedPositionFromCurrentPosition(Vector3 t_DeformDirection, bool t_ForceDeformation = false)
        {

            if (m_IsDeformationAlowed || t_ForceDeformation)
            {

                m_CurrentVertexPosition += t_DeformDirection;
                m_IsDeformationAlowed = false;
            }

            return m_CurrentVertexPosition;
        }

        public Vector3 GetDeformedPositionFromInitialPosition(Vector3 t_DeformDirection, bool t_ForceDeformation = false)
        {

            if (m_IsDeformationAlowed || t_ForceDeformation)
            {

                m_CurrentVertexPosition = m_InitialVertexPosition + t_DeformDirection;
                m_IsDeformationAlowed = false;
            }

            return m_CurrentVertexPosition;
        }
    }
}


