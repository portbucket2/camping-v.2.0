﻿namespace com.alphapotato.Gameplay {
    using System.Collections;
    using com.faithstudio.Gameplay;
    using UnityEngine;

    #region Custom Variables

    public enum CrowdMode {
        Idle,
        Cheer,
        Sad,
        Random
    }

    #endregion

    public class CrowdController : MonoBehaviour {

        #region Custom Variables

        [System.Serializable]
        public class CrowdGroup {
            public Transform crowdGroupParent;
            public CrowdBehaviour[] enlistedCrowdForThisGroup;
        }

        #endregion

        #region Public Variables

        public static CrowdController Instance;

        [Header ("Reference      :   External")]
        public LevelManager levelManagerReference;

        [Header ("Configuretion  :   CrowdHitAward")]
        [Range (0f, 1f)]
        public float crowdCoinEarnMultiplier;
        [Range (1, 10)]
        public int numberOfCoinToBeAwarded;
        public AnimationCurve coinAwardBasedOnLevelProgression;

        [Space (10.0f)]
        public AnimationCurve curveForSlowMotionOnHit;
        public AnimationCurve curveForRestoreMotionOnHit;

        [Header ("Configuretion  :   CrowdControl")]
        [Range (0, 10)]
        public int numberOfIdleAnimation;
        [Range (0, 10)]
        public int numberOfCheeringAnimation;
        [Range (0, 10)]
        public int numberOfSadAnimation;
        public CrowdGroup[] crowdGroups;

        #endregion

        #region Private Variables

        private string CROWD_GROUP_INDEX = "CROWD_GROUP_INDEX";

        private bool m_IsCrowdExpressionControllerRunning;
        private CrowdMode m_CurrentCrowdMode = CrowdMode.Idle;

        #endregion

        #region Mono Behaviour

        private void Awake () {

            Instance = this;
        }

        #endregion

        #region Configuretion

        private bool IsValidCrowdIndex (int t_CrowdIndex) {

            if (t_CrowdIndex >= 0 && t_CrowdIndex < crowdGroups.Length)
                return true;

            Debug.LogError ("Invalid CrowdIndex : " + t_CrowdIndex + ", Must be within [0," + crowdGroups.Length + ")");;
            return false;
        }

        private IEnumerator ControllerForCrowdExpression () {

            while (m_IsCrowdExpressionControllerRunning) {

                int t_NumberOfCrowd = crowdGroups[GetCurrentCrowdGroupIndex ()].enlistedCrowdForThisGroup.Length;
                for (int i = 0; i < t_NumberOfCrowd; i++) {

                    if (Random.Range (0f, 1f) <= 0.5f) {

                        switch (m_CurrentCrowdMode) {
                            case CrowdMode.Idle:
                                crowdGroups[GetCurrentCrowdGroupIndex ()].enlistedCrowdForThisGroup[i].PlayRandomIdleAnimation (numberOfIdleAnimation);
                                break;
                            case CrowdMode.Cheer:
                                crowdGroups[GetCurrentCrowdGroupIndex ()].enlistedCrowdForThisGroup[i].PlayRandomCheeringAnimation (numberOfCheeringAnimation);
                                break;
                            case CrowdMode.Sad:
                                crowdGroups[GetCurrentCrowdGroupIndex ()].enlistedCrowdForThisGroup[i].PlayRandomSadAnimation (numberOfSadAnimation);
                                break;
                            case CrowdMode.Random:
                                break;
                        }
                    } else {
                        crowdGroups[GetCurrentCrowdGroupIndex ()].enlistedCrowdForThisGroup[i].PlayRandomIdleAnimation (numberOfIdleAnimation);
                    }
                }

                yield return new WaitForSeconds (Random.Range (3, 5));
            }

            StopCoroutine (ControllerForCrowdExpression ());
        }

        #endregion

        #region Public Callback

        public int GetCurrentCrowdGroupIndex () {

            return PlayerPrefs.GetInt (CROWD_GROUP_INDEX, 0);
        }

        public void ActivateCrowd (CrowdMode t_CrowdMode = CrowdMode.Idle) {

            int t_CurrentCrowdIndex = GetCurrentCrowdGroupIndex ();
            int t_NumberOfCrowdGroup = crowdGroups.Length;

            for (int i = 0; i < t_NumberOfCrowdGroup; i++) {

                if (t_CurrentCrowdIndex == i) {

                    int t_EnlistedCrowdGroup = crowdGroups[i].enlistedCrowdForThisGroup.Length;

                    if (levelManagerReference != null) {

                        float t_LevelProgression = levelManagerReference.GetLevelProgression ();
                        int t_NumberOfCrowdToBeActiveInLevel = 50; //levelManagerReference.GetNumberOfCrowdForCurrentLevel();
                        int t_NumberOfCrowdPreProcessed = 0;
                        for (int j = 0; j < t_EnlistedCrowdGroup; j++) {
                            if (t_NumberOfCrowdPreProcessed < t_NumberOfCrowdToBeActiveInLevel && (Random.Range (0f, 1f) <= (0.5f + Mathf.Lerp (0f, 0.5f, t_LevelProgression)))) {

                                crowdGroups[i].enlistedCrowdForThisGroup[j].PreProcess (numberOfIdleAnimation);
                                t_NumberOfCrowdPreProcessed++;
                            } else {

                                crowdGroups[i].enlistedCrowdForThisGroup[j].PostProcess ();
                            }
                        }
                    } else {

                        for (int j = 0; j < t_EnlistedCrowdGroup; j++) {
                            crowdGroups[i].enlistedCrowdForThisGroup[j].PreProcess (numberOfIdleAnimation);
                        }
                    }
                } else {

                    int t_EnlistedCrowdGroup = crowdGroups[i].enlistedCrowdForThisGroup.Length;
                    for (int j = 0; j < t_EnlistedCrowdGroup; j++) {

                        crowdGroups[i].enlistedCrowdForThisGroup[j].PostProcess ();
                    }
                }
            }

            SwitchCrowdMode (t_CrowdMode);
        }

        public void SwitchCrowdMode (CrowdMode t_CrowdMode) {

            m_CurrentCrowdMode = t_CrowdMode;

            if (!m_IsCrowdExpressionControllerRunning) {

                m_IsCrowdExpressionControllerRunning = true;
                StartCoroutine (ControllerForCrowdExpression ());
            }
        }

        public void SwitchToCrowdIndex (int t_CrowdIndex) {

            if (IsValidCrowdIndex (t_CrowdIndex))
                PlayerPrefs.SetInt (CROWD_GROUP_INDEX, t_CrowdIndex);
        }

        public void GoToNextCrowdIndex () {

            int t_NextCrowdIndex = GetCurrentCrowdGroupIndex () + 1;

            if (t_NextCrowdIndex >= crowdGroups.Length)
                t_NextCrowdIndex = 0;

            SwitchToCrowdIndex (t_NextCrowdIndex);

        }

        public void GoToPreviousCrowdIndex () {

            int t_NextCrowdIndex = GetCurrentCrowdGroupIndex () - 1;
            if (t_NextCrowdIndex < 0)
                t_NextCrowdIndex = crowdGroups.Length - 1;

            SwitchToCrowdIndex (t_NextCrowdIndex);
        }

        public int GetNumberOfCoinToBeRewardedForHittingCrowd () {

            return (int) (1 + ((numberOfCoinToBeAwarded - 1) * coinAwardBasedOnLevelProgression.Evaluate (1f/*GameplayController.Instance.levelManagerReference.GetLevelProgression ()*/)));
        }

        public float GetCrowdCoinEarnMultiplier () {

            return /*GameplayController.Instance.playerControllerReference.GetCoinEarnMultiplier ()*/ 1f * crowdCoinEarnMultiplier;
        }

        #endregion
    }
}