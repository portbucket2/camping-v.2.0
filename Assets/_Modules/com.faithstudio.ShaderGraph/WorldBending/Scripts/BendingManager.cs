﻿using UnityEngine;
using UnityEngine.Rendering;

[ExecuteAlways]
public class BendingManager : MonoBehaviour
{

    #region Public Variables

    [Range(1,180)]
    public float sizeOfFrustrum = 100;

    #endregion

    #region Constant

    private const string WORLD_BENDING = "FS_WB_ENABLE_WORLD_BENDING";

    #endregion

    #region Mono Behaviour

    private void Awake(){

        if(Application.isPlaying){
            Shader.EnableKeyword(WORLD_BENDING);
        }else{
            Shader.DisableKeyword(WORLD_BENDING);
        }
    }

    private void OnEnable(){

        RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
        RenderPipelineManager.endCameraRendering += OnEndCameraRendering;
    }

    private void OnDisable(){

        RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
        RenderPipelineManager.endCameraRendering -= OnEndCameraRendering;
    }

    #endregion

    #region Configuretion

    private void OnBeginCameraRendering(
        ScriptableRenderContext t_ScriptableRenderContext,
        Camera t_Camera
    ){

        float t_NearClipingPlane    = t_Camera.nearClipPlane;
        float t_FarClipingPlane     = t_Camera.farClipPlane;
        t_Camera.cullingMatrix = Matrix4x4.Ortho(
            -sizeOfFrustrum,
            sizeOfFrustrum,
            -sizeOfFrustrum,
            sizeOfFrustrum,
            t_NearClipingPlane,
            t_FarClipingPlane) * t_Camera.worldToCameraMatrix;
    }

    private void OnEndCameraRendering(
        ScriptableRenderContext t_ScriptableRenderContext,
        Camera t_Camera
    ){

        t_Camera.ResetCullingMatrix();
    }

    #endregion
}
