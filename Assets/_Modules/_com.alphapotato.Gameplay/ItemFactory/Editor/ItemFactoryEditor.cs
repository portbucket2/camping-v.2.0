﻿namespace com.alphapotato.Gameplay {

    using UnityEditor;
    using UnityEngine;

    [CustomEditor (typeof (ItemFactory))]
    public class ItemFactoryEditor : Editor {

        private ItemFactory Reference;
        private void OnEnable () {
            Reference = (ItemFactory) target;
            Reference.DataInitialization ();
        }

        public override void OnInspectorGUI () {
            serializedObject.Update ();

            if (Reference.itemDatas != null) {

                int t_NumberOfItem = Reference.itemDatas.Length;
                if (t_NumberOfItem > 0) {

                    int t_SelectedItemIndex = Reference.GetSelectedItemIndex ();

                    EditorGUILayout.BeginHorizontal (); {
                        EditorGUILayout.LabelField (
                            "(" +
                            t_SelectedItemIndex +
                            ") " +
                            Reference.GetSelectedItemName () +
                            (Reference.IsItemPurchased (t_SelectedItemIndex) ? (" : Item Level (" + (Reference.GetSelectedItemLevel () + 0) + "/" + Reference.GetSelectedItemMaxLevel () + ")") : ""));

                        if (Reference.IsItemPurchased (t_SelectedItemIndex) && GUILayout.Button ("Reset Purchase")) {

                            Reference.ResetSelectedItemPurchase ();
                            int t_NumberOfSkill = Reference.GetNumberOfSkillForSelectedItem ();
                            for(int i = 0; i < t_NumberOfSkill; i++){
                                Reference.ResetSkillForSelectedItem(i);
                            }
                        }

                        if (GUILayout.Button ("Previous")) {

                            Reference.GoToPreviousItem ();
                        }

                        if (GUILayout.Button ("Next")) {

                            Reference.GoToNextItem ();
                        }
                    }
                    EditorGUILayout.EndHorizontal ();

                    for (int itemIndex = 0; itemIndex < t_NumberOfItem; itemIndex++) {

                        //Fixing    :   Name
                        if (Reference.GetItemName (itemIndex) == "") {

                            if (Reference.GetItemMesh (itemIndex) != null) {

                                Reference.itemDatas[itemIndex].itemName = Reference.GetItemMesh (itemIndex).name;
                            } else {

                                Reference.itemDatas[itemIndex].itemName = "Item (" + itemIndex + ")";
                            }
                        }

                        if (t_SelectedItemIndex == itemIndex) {

                            int t_NumberOfSkill = Reference.GetNumberOfSkillForSelectedItem ();

                            //Item State
                            EditorGUILayout.Space ();
                            EditorGUILayout.BeginHorizontal (); {
                                if (Reference.itemDatas[t_SelectedItemIndex].maxLevelToUpgrade > 0) {

                                    if (Reference.IsItemPurchased (t_SelectedItemIndex)) {
                                        EditorGUILayout.LabelField (
                                            "State : " +
                                            Reference.GetCurrentStateOfSelectedItem ().ToString ("F2") +
                                            ", UpgradeCost : " +
                                            Reference.GetUpgradeCostForSelectedItem ().ToString ("F2")
                                        );

                                        if (GUILayout.Button ("Upgrade")) {
                                            Reference.UpgradeSelectedItem ();
                                        }
                                        if (GUILayout.Button ("Downgrade")) {
                                            Reference.DowngradeSelectedItem ();
                                        }
                                        if (GUILayout.Button ("Reset")) {
                                            Reference.ResetSelectedItemLevel ();
                                        }
                                    } else {

                                        EditorGUILayout.LabelField (
                                            "State : " +
                                            Reference.GetCurrentStateOfSelectedItem ().ToString ("F2") +
                                            ", Purchase Cost : " +
                                            Reference.GetPurchaseCostForItem (t_SelectedItemIndex).ToString ("F2")
                                        );

                                        if (GUILayout.Button ("Purchase")) {
                                            Reference.PurchaseItem (t_SelectedItemIndex);
                                        }
                                    }

                                }

                            }
                            EditorGUILayout.EndHorizontal ();

                            //Item State
                            EditorGUILayout.Space ();

                            for (int skillIndex = 0; skillIndex < t_NumberOfSkill; skillIndex++) {

                                EditorGUI.indentLevel += 1;

                                //Check : If name field is empty
                                if (Reference.GetSelectedSkillName (skillIndex) == "") {
                                    Reference.itemDatas[t_SelectedItemIndex].itemSkills[skillIndex].skillName = "Skill(" + skillIndex + ")";
                                    Reference.DataInitialization ();
                                }

                                if (Reference.IsSkillUnlockedForSelectedItem (skillIndex)) {

                                    EditorGUILayout.LabelField (
                                        "(" +
                                        skillIndex +
                                        ") " +
                                        Reference.GetSelectedSkillName (skillIndex) +
                                        (" : SkillLevel (" + Reference.GetSkillLevelForSelectedItem (skillIndex) + "/" + Reference.GetSelectedItemMaxSkillLevel (skillIndex)) + ")");

                                    EditorGUI.indentLevel += 1;
                                    EditorGUILayout.BeginHorizontal (); {
                                        EditorGUILayout.LabelField (
                                            "State : " +
                                            Reference.GetSkillStateForSelectedItem (skillIndex).ToString ("F2") +
                                            ", UpgradeCost : " +
                                            Reference.GetUpgradeCostOfSkillForSelectedItem (skillIndex).ToString ("F2")
                                        );

                                        if (GUILayout.Button ("Upgrade")) {
                                            Reference.UpgradeSkillForSelectedItem (skillIndex);
                                        }
                                        if (GUILayout.Button ("Downgrade")) {
                                            Reference.DowngradeSkillForSelectedItem (skillIndex);
                                        }
                                        if (GUILayout.Button ("Reset")) {
                                            Reference.ResetSkillForSelectedItem (skillIndex);
                                        }
                                    }
                                    EditorGUILayout.EndHorizontal ();
                                    EditorGUI.indentLevel -= 1;
                                } else {

                                    EditorGUILayout.BeginHorizontal (); {
                                        EditorGUILayout.LabelField (
                                            "(" +
                                            skillIndex +
                                            ")" +
                                            Reference.GetSelectedSkillName (skillIndex) +
                                            ", UnlockCost : " + Reference.GetUnlockCostOfSkillForSelectedItem (skillIndex));

                                        if(GUILayout.Button("Unlock Skill")){

                                            Reference.UnlockSkillForSelectedItem(skillIndex);
                                        }
                                    }
                                    EditorGUILayout.EndHorizontal ();

                                }

                                //(" : SkillLevel (" + Reference.GetSkillLevelForSelectedItem(skillIndex).ToString("F0") + "/" + Reference.GetSelectedItemMaxSkillLevel(skillIndex).ToString("F0")

                                if (Reference.IsSkillUnlockedForSelectedItem (skillIndex)) {

                                }

                                EditorGUI.indentLevel -= 1;
                                EditorGUILayout.Space ();
                            }
                        }
                    }
                }
            }

            base.OnInspectorGUI ();

            serializedObject.ApplyModifiedProperties ();
        }
    }
}