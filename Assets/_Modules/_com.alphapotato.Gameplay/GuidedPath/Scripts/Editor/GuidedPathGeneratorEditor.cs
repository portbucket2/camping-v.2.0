﻿namespace com.alphapotato.Gameplay {

    using UnityEditor;
    using UnityEngine;
    using System.Linq;
    using System.Collections.Generic;

    [CustomEditor (typeof (GuidedPathGenerator))]
    public class GuidedPathGeneratorEditor : Editor {

        private GuidedPathGenerator Reference;

        private void OnEnable () {

            Reference = (GuidedPathGenerator) target;
        }

        public override void OnInspectorGUI () {

            serializedObject.Update ();

            EditorGUILayout.BeginHorizontal (); 
            {
                if (EditorApplication.isPlaying) {

                    if (GUILayout.Button ("Show GuidedPath (Default)")) {

                        Reference.ShowGuidedPath ();
                    }

                    if(GUILayout.Button("Hide GuidedPath")){
                        
                        Reference.HideGuidedPath();
                    }
                }

                if (GUILayout.Button ("Fetch Default GuidedPath")) {

                    if(Reference.parentForDefaultGuidedPath != null){
                        
                        List<Transform> t_TempList = Reference.parentForDefaultGuidedPath.GetComponentsInChildren<Transform>().ToList();
                        t_TempList.RemoveAt(0);
                        t_TempList.Add(t_TempList[0]);
                        t_TempList.TrimExcess();
                        Reference.defaultPointForGuidedPath = t_TempList;
                    }else{

                        Debug.LogError("Parent is not set");
                    }
                }
            }
            EditorGUILayout.EndHorizontal ();

            base.OnInspectorGUI ();

            serializedObject.ApplyModifiedProperties ();
        }

    }
}