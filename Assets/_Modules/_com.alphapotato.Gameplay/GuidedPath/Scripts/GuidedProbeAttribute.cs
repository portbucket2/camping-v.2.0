﻿namespace com.alphapotato.Gameplay {
    
    using UnityEngine;

    public class GuidedProbeAttribute : MonoBehaviour {
        #region Private Variables

        [HideInInspector]
        public float initialInterpolatedValue;

        public Collider colliderReference;
        public MeshRenderer[] meshRendererReference;

        #endregion

        #region Private Variables

        private bool m_IsMeshHidden = false;
        private Transform m_TransformReference;

        #endregion

        #region MonoBehaviour

        private void Awake () {
            m_TransformReference = transform;
        }

        #endregion

        #region Public Callback

        public bool IsMeshHidden () {

            return m_IsMeshHidden;
        }

        public Vector3 GetPosition () {

            return m_TransformReference.position;
        }

        public void SetPosition (Vector3 t_NewPosition) {

            m_TransformReference.position = t_NewPosition;
        }

        public void ShowMesh () {

            int t_NumberOfMeshRenderer = meshRendererReference.Length;
            colliderReference.enabled = true;
            for (int i = 0; i < t_NumberOfMeshRenderer; i++) {

                meshRendererReference[i].enabled = true;
            }

            m_IsMeshHidden = false;
        }

        public void HideMesh () {

            int t_NumberOfMeshRenderer = meshRendererReference.Length;
            for (int i = 0; i < t_NumberOfMeshRenderer; i++) {
                meshRendererReference[i].enabled = false;
            }
            colliderReference.enabled = false;
            m_IsMeshHidden = true;
        }

        #endregion
    }

}