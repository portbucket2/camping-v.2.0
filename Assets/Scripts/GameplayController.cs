﻿using System.Collections;
using UnityEngine;
using com.alphapotato.GameplayService;

public class GameplayController : MonoBehaviour
{
    #region Public Variables

    #if UNITY_EDITOR

    [Header("Parameter  :   Editor")]
    public bool continousLoopThroughLevel;

    #endif

    

    [Header("Reference  :   External")]
    public Camera mainCameraReference;
    public AdditiveSceneManager additiveSceneManagerReference;
    public LevelManager levelManagerReference;

    #endregion

    #region Private Variables

    private bool m_IsLevelAlreadyPlaying;

    #endregion

    #region Mono Behaviour

    private void Awake(){

        LoadEnvironmentForCurrentLevel();
    }

    private void Start(){

        //Later will shift to start button
        StartLevel();
    }

    #endregion

    #region Configuretion

    private void LoadEnvironmentForCurrentLevel(){
        
        additiveSceneManagerReference.LoadScene(levelManagerReference.GetAdditiveSceneNameForCurrentLevel());
    }

    private IEnumerator ControllerForLevelStateSequence(){

        int t_CurrentLevel = levelManagerReference.GetCurrentLevel();
        int t_NumberOfMiniGame = levelManagerReference.GetNumberOfMiniGameControllerForCurrentLevel();

        SubLevelManager t_NewSubLevel = Instantiate(
            levelManagerReference.levels[t_CurrentLevel].subLevelManagerReference,
            Vector3.zero,
            Quaternion.identity
        ).GetComponent<SubLevelManager>();

        t_NewSubLevel.transform.SetParent(levelManagerReference.transform);

        for(int miniGameIndex = 0; miniGameIndex < t_NumberOfMiniGame ; miniGameIndex++){

            bool t_IsMiniGameEnd = false;
            t_NewSubLevel.listOfMiniGame[miniGameIndex].mainCameraReference = mainCameraReference;
            t_NewSubLevel.listOfMiniGame[miniGameIndex].StartGame(delegate{

                    t_IsMiniGameEnd = true;
                }
            );

            WaitUntil t_WaitUntilMiniGameEnd = new WaitUntil(() => {

                if(!t_IsMiniGameEnd)
                    return false;

                return true;
            });
            yield return t_WaitUntilMiniGameEnd;
        }
        
        Destroy(t_NewSubLevel.gameObject);

        m_IsLevelAlreadyPlaying = false;
        StopCoroutine(ControllerForLevelStateSequence());
        #if UNITY_EDITOR

        if(continousLoopThroughLevel){
            levelManagerReference.IncreaseLevel();
            StartLevel();
        }

        #endif
    }

    #endregion

    #region Public Callback

    public void StartLevel(){

        if(!m_IsLevelAlreadyPlaying){

            LoadEnvironmentForCurrentLevel();

            m_IsLevelAlreadyPlaying = true;
            StartCoroutine(ControllerForLevelStateSequence());
        }
    }

    #endregion
    
}
