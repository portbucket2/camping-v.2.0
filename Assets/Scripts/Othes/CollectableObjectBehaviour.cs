﻿using UnityEngine;
using UnityEngine.Events;

public class CollectableObjectBehaviour : MonoBehaviour {
    #region Public Variables

    public ParticleSystem collectionParticle;
    public Animator animatorReference;

    #endregion

    #region Private Variables

    private bool        m_IsAllowedToCollect;
    private UnityAction OnCollect;
    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void PreProcess (UnityAction OnCollect = null) {

        m_IsAllowedToCollect    = false;
        this.OnCollect          = OnCollect;
    }

    public void ShowCollectingEffect () {

        if (!m_IsAllowedToCollect) {
            
            m_IsAllowedToCollect = true;

            if (collectionParticle != null)
                collectionParticle.Play ();

            if (animatorReference != null)
                animatorReference.SetTrigger ("COLLECT");

            OnCollect?.Invoke();
        }

    }

    #endregion
}