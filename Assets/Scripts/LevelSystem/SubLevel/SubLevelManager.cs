﻿
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR

using UnityEditor;

#endif

public class SubLevelManager : MonoBehaviour
{
    #region Public Varaibles

    #if UNITY_EDITOR

    [HideInInspector]
    public bool showDefaultInspector = false;
    //[HideInInspector]
    //public string scenePath;
    //[HideInInspector]
    //public SceneAsset sceneAsset;

    #endif

    public string sceneName;
    public List<MiniGameController> listOfMiniGame;

    #endregion

    #region Public Callback

     public bool IsValidIndexForMiniGame(int t_MiniGameIndex){

        if(t_MiniGameIndex >= 0 && t_MiniGameIndex < listOfMiniGame.Count)
            return true;

        Debug.LogError("Invalid MiniGameIndex (" + t_MiniGameIndex + ")");

        return false;
    }

    #endregion
}
