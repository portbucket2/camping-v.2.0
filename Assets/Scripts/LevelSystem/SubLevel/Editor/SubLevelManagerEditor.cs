﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor (typeof (SubLevelManager))]
public class SubLevelManagerEditor : Editor {
    private SubLevelManager Reference;
    private ReorderableList m_MiniGameList;

    private void OnEnable () {

        if (target == null)
            return;

        Reference = (SubLevelManager) target;

        m_MiniGameList = new ReorderableList (
            serializedObject,
            serializedObject.FindProperty ("listOfMiniGame")) {
            displayAdd = true,
                displayRemove = true,
                draggable = true,
                drawHeaderCallback = rect => {
                    EditorGUI.LabelField (
                        new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                        "MiniGames");
                },
                drawElementCallback = (rect, index, isActive, isFocused) => {

                    SerializedProperty t_MiniGameController = serializedObject.FindProperty ("listOfMiniGame").GetArrayElementAtIndex (index);
                    EditorGUI.ObjectField (
                        new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                        t_MiniGameController
                    );
                },
                elementHeightCallback = index => {
                    return 1 * EditorGUIUtility.singleLineHeight;
                }
        };
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        Reference.showDefaultInspector = EditorGUILayout.Toggle (
            "ShowDefaultInspector",
            Reference.showDefaultInspector
        );

        DrawHorizontalLine ();

        if (Reference.showDefaultInspector) {

            DrawDefaultInspector ();
        } else {

            // SceneAsset t_NewSceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset> (Reference.scenePath);
            // EditorGUI.BeginChangeCheck ();
            // Object t_NewSceneObject = EditorGUILayout.ObjectField (
            //     "SceneAsset",
            //     t_NewSceneAsset,
            //     typeof (SceneAsset), false) as SceneAsset;
            // if (EditorGUI.EndChangeCheck ()) {

            //     string t_NewSceneDataPath = AssetDatabase.GetAssetPath (t_NewSceneObject);
            //     SerializedProperty t_NewScenePathProperty = serializedObject.FindProperty("scenePath");
            //     SerializedProperty t_NewSceneNameProperty = serializedObject.FindProperty("sceneName");
            //     t_NewScenePathProperty.stringValue = t_NewSceneDataPath;

            //     string[] t_SplitByDash = t_NewSceneDataPath.Split ('/');
            //     string[] t_SplitByDot = t_SplitByDash[t_SplitByDash.Length - 1].Split ('.');
            //     t_NewSceneNameProperty.stringValue = t_SplitByDot[0];
            // }

            m_MiniGameList.DoLayoutList ();
        }

        serializedObject.ApplyModifiedProperties ();
    }

    #region Editor Moduler Function

    private void DrawHorizontalLine () {

        EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
    }

    private void DrawSettingsEditor (Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

        if (settings != null) {

            using (var check = new EditorGUI.ChangeCheckScope ()) {

                foldout = EditorGUILayout.InspectorTitlebar (foldout, settings);

                if (foldout) {

                    CreateCachedEditor (settings, null, ref editor);
                    editor.OnInspectorGUI ();

                    if (check.changed) {

                        if (OnSettingsUpdated != null) {

                            OnSettingsUpdated.Invoke ();
                        }
                    }
                }
            }
        }
    }

    #endregion

}