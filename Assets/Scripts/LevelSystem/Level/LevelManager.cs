﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelManager : MonoBehaviour {
    #region Custom Variables

    [System.Serializable]
    public struct Level {

#if UNITY_EDITOR

        public bool         showLevelInfo;
        public string       scenePath;
        public SceneAsset   environmentScene;
#endif
        
        public string sceneName;
        public GameObject subLevelManagerReference;
    }

    #endregion

    #region Public Varaibles

    #if UNITY_EDITOR
    
    public bool showDefaultInspector;

    #endif

    public List<Level> levels;

    #endregion

    #region Private Variables

    private string PP_LEVEL_TRACKER = "PPK_LEVEL_TRACKER";

    #endregion

    #region Public Callback :   Level

    public bool IsValidLevel (int t_Level) {

        if (t_Level >= 0 && t_Level < levels.Count) {

            return true;
        } else {

            return false;
        }
    }

    public int GetCurrentLevel () {

        return PlayerPrefs.GetInt (PP_LEVEL_TRACKER, 0);
    }

    public void IncreaseLevel (int t_IncrementValue = 1) {

        int t_CurrentLevel = GetCurrentLevel ();

#if UNITY_IOS
        //FacebookAnalyticsManager.Instance.FBALevelComplete(t_CurrentLevel);
        //FirebaseAnalyticsEventController.Instance.UpdateGameProgression("Level Achieved", t_CurrentLevel);
#endif

        int t_NewLevel = t_CurrentLevel + t_IncrementValue;
        if (t_NewLevel >= levels.Count) {
            t_NewLevel = 0;
            Debug.Log("Reset Hoitese 1");
        }

        PlayerPrefs.SetInt (PP_LEVEL_TRACKER, t_NewLevel);
        //UIStateController.Instance.UpdateLevelInfo(t_NewLevel);
        //UIRateUsController.Instance.AskUserForReview();
    }

    public void DecreaseLevel (int t_DecrementValue = 1) {

        int t_NewLevel = GetCurrentLevel () - t_DecrementValue;
        if (t_NewLevel < 0) {
            PlayerPrefs.SetInt (PP_LEVEL_TRACKER, levels.Count - 1);
        } else {

            PlayerPrefs.SetInt (PP_LEVEL_TRACKER, t_NewLevel);
        }

        Debug.Log("Reset Hoitese 2 ");
    } 

    public float GetLevelProgression () {
        return (GetCurrentLevel () / ((float) levels.Count));
    }

    public void ResetLevel () {
        PlayerPrefs.SetInt (PP_LEVEL_TRACKER, 0);

        Debug.Log("Reset Hoitese 3");
    }

    #endregion

    #region Public Callback :   MiniGame

    public bool IsValidIndexForMiniGame(int t_Level, int t_MiniGameIndex){

        if(IsValidLevel(t_Level))
            return levels[t_Level].subLevelManagerReference.GetComponent<SubLevelManager>().IsValidIndexForMiniGame(t_MiniGameIndex);

        Debug.LogError("Invalid MiniGameIndex (" + t_MiniGameIndex + ")");

        return false;
    }

    public int GetNumberOfMiniGameControllerForCurrentLevel(){

        return GetNumberOfMiniGameController(GetCurrentLevel());
    }

    public int GetNumberOfMiniGameController(int t_Level){

        if(IsValidLevel(t_Level)){

            return levels[t_Level].subLevelManagerReference.GetComponent<SubLevelManager>().listOfMiniGame.Count;
        }

        return 0;
    }

    public string GetAdditiveSceneNameForCurrentLevel(){

        return GetAdditiveSceneName(GetCurrentLevel());
    }

    public string GetAdditiveSceneName(int t_Level){

        if(IsValidLevel(t_Level)){

            return levels[t_Level].sceneName;
        }

        return "Invalid Level";
    }

    public MiniGameController GetMiniGameControllerForCurrentLevel(int t_MiniGameIndex){

        return GetMiniGameController(GetCurrentLevel(), t_MiniGameIndex);
    }   

    public MiniGameController GetMiniGameController(int t_Level, int t_MiniGameIndex){

        if(IsValidIndexForMiniGame(t_Level, t_MiniGameIndex)){

            if(levels[t_Level].subLevelManagerReference.GetComponent<SubLevelManager>().IsValidIndexForMiniGame(t_MiniGameIndex))
                return levels[t_Level].subLevelManagerReference.GetComponent<SubLevelManager>().listOfMiniGame[t_MiniGameIndex];
        }

        return null;
    }

    #endregion
}