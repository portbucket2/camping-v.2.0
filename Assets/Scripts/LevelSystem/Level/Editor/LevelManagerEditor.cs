﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor (typeof (LevelManager))]
public class LevelManagerEditor : Editor {

    private Editor characterFactoryEditor;
    private LevelManager Reference;

    private ReorderableList m_LevelInfo;
    private Dictionary<string, ReorderableList> m_SubLevelInfo = new Dictionary<string, ReorderableList> ();

    private void OnEnable () {

        if (target == null)
            return;

        Reference = (LevelManager) target;

        m_LevelInfo = new ReorderableList (serializedObject, serializedObject.FindProperty ("levels")) {
            displayAdd = true,
                displayRemove = true,
                draggable = true,
                drawHeaderCallback = rect => {
                    EditorGUI.LabelField (new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "Level");
                },
                drawElementCallback = (rect, index, a, h) => {

                    var element = serializedObject.FindProperty ("levels").GetArrayElementAtIndex (index);

                    EditorGUI.PropertyField (
                        new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative ("subLevelManagerReference")
                    );

                    EditorGUI.BeginChangeCheck ();
                    EditorGUI.ObjectField (
                        new Rect (rect.x, rect.y + (EditorGUIUtility.singleLineHeight * 1), rect.width, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative ("environmentScene")
                    );

                    if (EditorGUI.EndChangeCheck ()) {

                        string t_NewSceneDataPath = AssetDatabase.GetAssetPath (element.FindPropertyRelative ("environmentScene").objectReferenceValue);
                        SerializedProperty t_NewScenePathProperty = element.FindPropertyRelative ("scenePath");
                        SerializedProperty t_NewSceneNameProperty = element.FindPropertyRelative ("sceneName");
                        t_NewScenePathProperty.stringValue = t_NewSceneDataPath;

                        string[] t_SplitByDash = t_NewSceneDataPath.Split ('/');
                        string[] t_SplitByDot = t_SplitByDash[t_SplitByDash.Length - 1].Split ('.');
                        t_NewSceneNameProperty.stringValue = t_SplitByDot[0];

                        Debug.Log ("AssetDataPath : " + t_NewSceneDataPath + ", SceneName : " + t_NewSceneNameProperty.stringValue);
                    }

                    EditorGUI.LabelField (
                        new Rect (rect.x, rect.y + (EditorGUIUtility.singleLineHeight * 2), rect.width, EditorGUIUtility.singleLineHeight),
                        "",
                        GUI.skin.horizontalSlider
                    );

                    //Inner ReorderList
                    // var InnerList = element.FindPropertyRelative ("subLevels");
                    // string listKey = element.propertyPath;
                    // ReorderableList innerReorderableList;
                    // if (m_SubLevelInfo.ContainsKey (listKey)) {
                    //     innerReorderableList = m_SubLevelInfo[listKey];
                    // } else {
                    //     innerReorderableList = new ReorderableList (element.serializedObject, InnerList) {
                    //         displayAdd = true,
                    //             displayRemove = true,
                    //             draggable = true,
                    //             drawHeaderCallback = innerRect => {

                    //                 EditorGUI.LabelField (innerRect, "Level (" + index + ")");
                    //             },
                    //             drawElementCallback = (innerRect, innerIndex, innerA, innerH) => {

                    //                 var innerElement = InnerList.GetArrayElementAtIndex (innerIndex);

                    //                 EditorGUI.PropertyField (innerRect, innerElement.FindPropertyRelative ("subLevelManagerReference"));
                    //             }
                    //     };
                    //     m_SubLevelInfo[listKey] = innerReorderableList;
                    // }

                    // var height = (InnerList.arraySize + 3) * EditorGUIUtility.singleLineHeight;
                    // innerReorderableList.DoList (new Rect (rect.x, rect.y + EditorGUIUtility.singleLineHeight, rect.width, height));
                },
                elementHeightCallback = index => {
                    var element = serializedObject.FindProperty ("levels").GetArrayElementAtIndex (index);
                    return EditorGUIUtility.singleLineHeight * 3;
                    //var innerList = element.FindPropertyRelative ("subLevels");
                    //return 1 + (innerList.arraySize + 4) * EditorGUIUtility.singleLineHeight;
                }

        };

    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        Reference.showDefaultInspector = EditorGUILayout.Toggle (
            "ShowDefaultEditor",
            Reference.showDefaultInspector);
        DrawHorizontalLine ();

        if (Reference.showDefaultInspector) {

            DrawHorizontalLine ();
            DrawDefaultInspector ();
        } else {

            EditorGUILayout.Space ();
            LevelValueModifier ();
            DrawHorizontalLine ();

            EditorGUILayout.Space ();
            m_LevelInfo.DoLayoutList ();
        }

        //DrawSettingsEditor(Reference.characterFactory, null, ref Reference.showCharacterFactoryOnEditor, ref characterFactoryEditor);

        serializedObject.ApplyModifiedProperties ();
    }

    #region EditorSection

    private void LevelValueModifier () {

        int t_CurrentLevel = Reference.GetCurrentLevel ();

        EditorGUILayout.BeginHorizontal (); {
            EditorGUILayout.LabelField ("CurrentLevel (" + t_CurrentLevel + ")");
            if (GUILayout.Button ("Increase Level")) {
                Reference.IncreaseLevel ();
            }
            if (GUILayout.Button ("Reset Level")) {
                Reference.ResetLevel ();
            }
        }
        EditorGUILayout.EndHorizontal ();

    }

    #endregion

    #region Editor Moduler Function

    private void DrawHorizontalLine () {

        EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
    }

    private void DrawSettingsEditor (Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

        if (settings != null) {

            using (var check = new EditorGUI.ChangeCheckScope ()) {

                foldout = EditorGUILayout.InspectorTitlebar (foldout, settings);

                if (foldout) {

                    CreateCachedEditor (settings, null, ref editor);
                    editor.OnInspectorGUI ();

                    if (check.changed) {

                        if (OnSettingsUpdated != null) {

                            OnSettingsUpdated.Invoke ();
                        }
                    }
                }
            }
        }
    }

    #endregion

}