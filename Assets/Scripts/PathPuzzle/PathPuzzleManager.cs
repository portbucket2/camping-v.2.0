﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using com.faithstudio.Gameplay;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class PathPuzzleManager : MonoBehaviour {

    #region Custom Variables

    public enum GameRules {
        movableAtAdjacent,
        movableAnywhere
    }

    public class PathNode {

        public int absoluteIndex;
        public Vector2 gridIndex;
        public PathTile pathTileReference;
    }

    [System.Serializable]
    public class InteractablePlaneInfo {

        public bool onDragging;
        public InteractablePlaneBehaviour interactablePlaneReference;
        public PathTile pathTileReference;
    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    [Header ("Parameter  :   Editor")]
    public bool showInteractableList;
    public bool isOriginWillBeResetForPathTile;
    public Color colorOfActiveArea;
    public Color colorOfInactiveArea;
#endif

    [Header ("Parameters")]
    public GameRules gameRules;
    [Range (1, 5f)]
    public float sizeForUnit = 1;
    [Range (0, 16)]
    public int areaOfPuzzle = 1;

    public LayerMask layerOfInteractableTile;

    public List<InteractablePlaneInfo> m_ListOfInteractablePlane;
    

    #endregion

    #region Private Variables

    private Camera m_MainCameraReference;

    private bool m_IsUserStartedInteracting;

    private int     m_IndexOfSelectedInteractableTile;
    private PathTile m_SelectedPathTile;
    private Transform m_TransformReferenceOfSelectedPathTile;

    [SerializeField]
    private List<PathTile> m_ResultedPathTile;
    [SerializeField]
    private List<Transform> m_Path;

    #endregion

    #region Mono Behaviour

    private void Start () {

        m_MainCameraReference = Camera.main;
        PreProcess ();
    }

#if UNITY_EDITOR

    private void OnDrawGizmosSelected () {

        Vector3 t_OriginPosition = transform.position;
        float t_LengthOfASide = areaOfPuzzle * sizeForUnit;
        float t_StartingPointAlongTheRow = -(t_LengthOfASide / 2.0f) + (sizeForUnit / 2.0f);
        Gizmos.matrix = this.transform.localToWorldMatrix;
        Gizmos.color = colorOfActiveArea;
        for (int i = 0; i < areaOfPuzzle; i++) {

            float t_StartingPointAlongTheColumn = -(t_LengthOfASide / 2.0f) + (sizeForUnit / 2.0f);
            for (int j = 0; j < areaOfPuzzle; j++) {

                if (m_ListOfInteractablePlane != null) {

                    int t_AbsoluteIndex = (i * areaOfPuzzle) + j;
                    if (m_ListOfInteractablePlane[t_AbsoluteIndex].pathTileReference != null && m_ListOfInteractablePlane[t_AbsoluteIndex].pathTileReference.onDraggingAllowed)
                        Gizmos.color = colorOfActiveArea;
                    else {

                        Gizmos.color = colorOfInactiveArea;
                        Gizmos.DrawCube (
                            new Vector3 (
                                t_StartingPointAlongTheRow + (i * sizeForUnit),
                                0f,
                                t_StartingPointAlongTheColumn + (j * sizeForUnit)),
                            new Vector3 (0.75f, 0.25f, 0.75f) * sizeForUnit
                        );
                    }
                }

            }
        }

        Gizmos.color = Color.blue;
        int t_NumberOfPath = m_Path.Count;
        for (int i = 0; i < t_NumberOfPath; i++) {
            Gizmos.DrawSphere (
                m_Path[i].position,
                0.25f
            );
        }
    }

#endif

    #endregion

    #region Configuretion

    private void Shuffle(){

        int t_NumberOfInteractablePlane             = m_ListOfInteractablePlane.Count;
        List<int> t_DraggableInteractablePlaneIndex = new List<int>();
        for(int i  = 0 ; i < t_NumberOfInteractablePlane; i++){

            if(m_ListOfInteractablePlane[i].pathTileReference != null && m_ListOfInteractablePlane[i].pathTileReference.onDraggingAllowed){

                t_DraggableInteractablePlaneIndex.Add(i);
            }
        }

        List<int> t_CopyOfDraggableInteractablePlaneIndex = new List<int>(t_DraggableInteractablePlaneIndex);
        int t_NumberOfDraggableInteractablePlane = t_DraggableInteractablePlaneIndex.Count;
        for(int i = 0; i< t_NumberOfDraggableInteractablePlane; i++){
            
            int t_Index1 = t_DraggableInteractablePlaneIndex[i];
            int t_Index2 = t_CopyOfDraggableInteractablePlaneIndex[Random.Range(0,t_CopyOfDraggableInteractablePlaneIndex.Count)];
            if(t_Index1 != t_Index2){
                
                InteractablePlaneInfo t_Temp = m_ListOfInteractablePlane[t_Index1];
                m_ListOfInteractablePlane[t_Index1] = m_ListOfInteractablePlane[t_Index2];
                m_ListOfInteractablePlane[t_Index2] = t_Temp;

                t_DraggableInteractablePlaneIndex.Remove(t_Index1);
                t_DraggableInteractablePlaneIndex.Remove(t_Index2);

                t_CopyOfDraggableInteractablePlaneIndex.Remove(t_Index1);
                t_CopyOfDraggableInteractablePlaneIndex.Remove(t_Index2);

                t_NumberOfDraggableInteractablePlane -= 2;
                i -= 2;
                i = i < 0 ? 0 : i;                
            }
        }

        RePositionInteractablePlane();  
    }

    private bool IsPuzzleSolvedForPathTile(){
        
        int t_NumberOfPathTile = m_ListOfInteractablePlane.Count;
        for(int i = 0; i < t_NumberOfPathTile; i++){

            if(m_ListOfInteractablePlane[i].pathTileReference != null && (m_ListOfInteractablePlane[i].pathTileReference != m_ResultedPathTile[i]))
                return false;
        }

        return true;
    }

    private void OnTouchDown (Vector3 t_TouchPosition) {

        if (!m_IsUserStartedInteracting) {
            m_IsUserStartedInteracting = true;
            StartCoroutine (ControllerForMovingTile ());
        }
        SelectInteractableTile (t_TouchPosition);
    }

    private void OnTouch (Vector3 t_TouchPosition) {

        DragInteractableTile (t_TouchPosition);
    }

    private void OnTouchUp (Vector3 t_TouchPosition) {

        DeselectInteractableTile (t_TouchPosition);

        m_IsUserStartedInteracting = false;
    }

    public void SelectInteractableTile (Vector3 t_TouchPosition) {

        Ray t_Ray;

#if UNITY_EDITOR

        if (EditorApplication.isPlaying) {

            t_Ray = m_MainCameraReference.ScreenPointToRay (t_TouchPosition);

        } else {

            t_Ray = Camera.current.ScreenPointToRay (t_TouchPosition);
        }

#else

        t_Ray = m_MainCameraReference.ScreenPointToRay (t_TouchPosition);

#endif

        RaycastHit t_RayCastHit;

        if (Physics.Raycast (t_Ray, out t_RayCastHit, 1000, layerOfInteractableTile)) {

            InteractablePlaneBehaviour t_InteractablePlane = t_RayCastHit.collider.GetComponent<InteractablePlaneBehaviour> ();
            if (t_InteractablePlane != null) {

                if (m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference.onDraggingAllowed) {

                    if (m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].pathTileReference != null) {

                        m_IndexOfSelectedInteractableTile = t_InteractablePlane.GetIndexOfInteractableTile ();
                        m_SelectedPathTile = m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].pathTileReference;

                        m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].onDragging = true;
                        m_TransformReferenceOfSelectedPathTile = m_SelectedPathTile.transform;
                    }
                }

            }
        }
    }

    public void DragInteractableTile (Vector3 t_TouchPosition) {

        Ray t_Ray;

#if UNITY_EDITOR

        if (EditorApplication.isPlaying) {

            t_Ray = m_MainCameraReference.ScreenPointToRay (t_TouchPosition);

        } else {

            t_Ray = Camera.current.ScreenPointToRay (t_TouchPosition);
        }

#else

        t_Ray = m_MainCameraReference.ScreenPointToRay (t_TouchPosition);

#endif

        RaycastHit t_RayCastHit;

        if (Physics.Raycast (t_Ray, out t_RayCastHit, 1000, layerOfInteractableTile)) {

            InteractablePlaneBehaviour t_InteractablePlane = t_RayCastHit.collider.GetComponent<InteractablePlaneBehaviour> ();
            if (t_InteractablePlane != null) {

                if (m_TransformReferenceOfSelectedPathTile != null) {

                    if (m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference.onDraggingAllowed &&
                        m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference != null &&
                        (m_SelectedPathTile != m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference)) {

                        m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].pathTileReference = m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference;
                        m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].pathTileReference.transform.SetParent (m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].interactablePlaneReference.transform);
                        m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].onDragging = false;

                        m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference = m_SelectedPathTile;
                        m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference.transform.SetParent (m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].interactablePlaneReference.transform);
                        m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].onDragging = true;

                        m_IndexOfSelectedInteractableTile = t_InteractablePlane.GetIndexOfInteractableTile ();
                    }

                    m_TransformReferenceOfSelectedPathTile.position = t_RayCastHit.point + (Vector3.up * sizeForUnit);
                }

            }
        }
    }

    public void DeselectInteractableTile (Vector3 t_TouchPosition) {

        Ray t_Ray;

#if UNITY_EDITOR

        if (EditorApplication.isPlaying) {

            t_Ray = m_MainCameraReference.ScreenPointToRay (t_TouchPosition);

        } else {

            t_Ray = Camera.current.ScreenPointToRay (t_TouchPosition);
        }

#else

        t_Ray = m_MainCameraReference.ScreenPointToRay (t_TouchPosition);

#endif

        RaycastHit t_RayCastHit;

        if (Physics.Raycast (t_Ray, out t_RayCastHit, 1000, layerOfInteractableTile)) {

            if (m_TransformReferenceOfSelectedPathTile != null) {

                InteractablePlaneBehaviour t_InteractablePlane = t_RayCastHit.collider.GetComponent<InteractablePlaneBehaviour> ();
                if (t_InteractablePlane != null &&
                    m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference.onDraggingAllowed &&
                    m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference == null) {

                    m_ListOfInteractablePlane[t_InteractablePlane.GetIndexOfInteractableTile ()].pathTileReference = m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].pathTileReference;

                    m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].pathTileReference = null;

                    m_TransformReferenceOfSelectedPathTile.SetParent (t_InteractablePlane.transform);
                } else {

                    m_TransformReferenceOfSelectedPathTile.SetParent (m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].interactablePlaneReference.transform);
                }
            }
        } else {

            if (m_TransformReferenceOfSelectedPathTile != null)
                m_TransformReferenceOfSelectedPathTile.SetParent (m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].interactablePlaneReference.transform);
        }

        if (m_TransformReferenceOfSelectedPathTile != null) {

            m_TransformReferenceOfSelectedPathTile.localPosition = Vector3.zero;
            m_TransformReferenceOfSelectedPathTile = null;
        }

        m_ListOfInteractablePlane[m_IndexOfSelectedInteractableTile].onDragging = false;

        Debug.Log("IsEnd : " + IsPuzzleSolvedForPathTile());
    }

    private IEnumerator ControllerForMovingTile () {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds (t_CycleLength);

        int t_NumberOfInteractablePlane = m_ListOfInteractablePlane.Count;

        while (m_IsUserStartedInteracting) {

            for (int i = 0; i < t_NumberOfInteractablePlane; i++) {

                if (!m_ListOfInteractablePlane[i].onDragging && m_ListOfInteractablePlane[i].pathTileReference != null) {

                    Vector3 t_ModifiedPosition = Vector3.Lerp (
                        m_ListOfInteractablePlane[i].pathTileReference.transform.localPosition,
                        Vector3.zero,
                        0.1f
                    );
                    m_ListOfInteractablePlane[i].pathTileReference.transform.localPosition = t_ModifiedPosition;
                }
            }
            yield return t_CycleDelay;
        }

        for (int i = 0; i < t_NumberOfInteractablePlane; i++)
            if (m_ListOfInteractablePlane[i].pathTileReference != null)
                m_ListOfInteractablePlane[i].pathTileReference.transform.localPosition = Vector3.zero;

        StopCoroutine (ControllerForMovingTile ());
    }

    #endregion

    #region Public Callback

    public void PreProcess () {

        GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
        GlobalTouchController.Instance.OnTouch += OnTouch;
        GlobalTouchController.Instance.OnTouchUp += OnTouchUp;

        Shuffle();
    }

    public void PostProcess () {

        GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
        GlobalTouchController.Instance.OnTouch -= OnTouch;
        GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;
    }

    public void RePositionInteractablePlane () {

        float t_LengthOfASide = areaOfPuzzle * sizeForUnit;
        float t_StartingPointAlongTheRow = -(t_LengthOfASide / 2.0f) + (sizeForUnit / 2.0f);
        for (int i = 0; i < areaOfPuzzle; i++) {

            float t_StartingPointAlongTheColumn = -(t_LengthOfASide / 2.0f) + (sizeForUnit / 2.0f);
            for (int j = 0; j < areaOfPuzzle; j++) {

                int t_AbsoluteIndex = (i * areaOfPuzzle) + j;

                m_ListOfInteractablePlane[t_AbsoluteIndex].interactablePlaneReference.PreProcess (t_AbsoluteIndex);
                
                #if UNITY_EDITOR
                m_ListOfInteractablePlane[t_AbsoluteIndex].interactablePlaneReference.gameObject.name = "InteractablePlane (" + t_AbsoluteIndex + ") : (" + i + "," + j + ")";
                #endif

                m_ListOfInteractablePlane[t_AbsoluteIndex].interactablePlaneReference.transform.SetSiblingIndex (t_AbsoluteIndex);
                m_ListOfInteractablePlane[t_AbsoluteIndex].interactablePlaneReference.transform.localPosition = new Vector3 (
                    t_StartingPointAlongTheRow + (i * sizeForUnit),
                    0f,
                    t_StartingPointAlongTheColumn + (j * sizeForUnit));
            }
        }
    }

#if UNITY_EDITOR

    public bool IsNodeAIsAdjacentOfNodeB (Vector2 t_GridIndexOfNodaA, Vector2 t_GridIndexOfNodaB) {

        if (Mathf.Abs (t_GridIndexOfNodaA.x - t_GridIndexOfNodaB.x) <= 1 && Mathf.Abs (t_GridIndexOfNodaA.y - t_GridIndexOfNodaB.y) <= 1)
            return true;

        return false;
    }

    public void InitializeInteractablePlane () {

        if (m_ListOfInteractablePlane == null)
            m_ListOfInteractablePlane = new List<InteractablePlaneInfo> ();

        int t_NumberOfNewInteractablePlane = (int) (Mathf.Pow (areaOfPuzzle, 2) - m_ListOfInteractablePlane.Count);
        if (t_NumberOfNewInteractablePlane > 0) {

            int t_NewSizeOfInteractableList = (int) (Mathf.Pow (areaOfPuzzle, 2));
            for (int i = m_ListOfInteractablePlane.Count; i < t_NewSizeOfInteractableList; i++) {

                GameObject t_NewInteractablePlane = new GameObject ();
                t_NewInteractablePlane.layer = Mathf.RoundToInt (Mathf.Log (layerOfInteractableTile, 2));

                Transform t_TransformReference = t_NewInteractablePlane.transform;
                t_TransformReference.SetParent (transform);

                BoxCollider t_BoxColliderReference = t_NewInteractablePlane.AddComponent<BoxCollider> ();
                Vector3 t_SizeOfBoxCollider = new Vector3 (
                    sizeForUnit,
                    sizeForUnit,
                    sizeForUnit
                );
                t_BoxColliderReference.size = t_SizeOfBoxCollider;

                InteractablePlaneBehaviour t_NewInteractablePlaneBehaviour = t_NewInteractablePlane.AddComponent<InteractablePlaneBehaviour> ();
                t_NewInteractablePlaneBehaviour.PreProcess (i);

                m_ListOfInteractablePlane.Add (new InteractablePlaneInfo () {
                    interactablePlaneReference = t_NewInteractablePlaneBehaviour
                });
            }
        } else {

            int t_NumberOfPlaneToBeRemoved = Mathf.Abs (t_NumberOfNewInteractablePlane);
            for (int i = m_ListOfInteractablePlane.Count - 1; t_NumberOfPlaneToBeRemoved > 0; t_NumberOfPlaneToBeRemoved--, i--) {

                if (m_ListOfInteractablePlane[i].interactablePlaneReference != null)
                    DestroyImmediate (m_ListOfInteractablePlane[i].interactablePlaneReference.gameObject);

                m_ListOfInteractablePlane.RemoveAt (i);
            }
        }

        ResizeInteractablePlane ();
    }

    public void ResizeInteractablePlane () {

        int t_NumberOFInteractablePlane = m_ListOfInteractablePlane.Count;
        for (int i = 0; i < t_NumberOFInteractablePlane; i++) {

            m_ListOfInteractablePlane[i]
                .interactablePlaneReference
                .GetComponent<BoxCollider> ().size = new Vector3 (
                    sizeForUnit,
                    0.1f,
                    sizeForUnit
                );
        }

        RePositionInteractablePlane ();
    }

    public void GeneratePath () {

        PathNode t_EndNode = new PathNode ();
        List<PathNode> t_ListOfRoadNode = new List<PathNode> ();

        int t_NumberOfInteractablePlane = m_ListOfInteractablePlane.Count;
        for (int i = 0; i < t_NumberOfInteractablePlane; i++) {

            if (m_ListOfInteractablePlane[i].pathTileReference != null) {

                if (m_ListOfInteractablePlane[i].pathTileReference.typeOfPathTile == PathTile.TypeOfPathTile.startTile) {
                    t_ListOfRoadNode.Add (new PathNode () {
                        absoluteIndex = i,
                            gridIndex = new Vector2 (
                                i / areaOfPuzzle,
                                i % areaOfPuzzle
                            ),
                            pathTileReference = m_ListOfInteractablePlane[i].pathTileReference
                    });
                } else if (m_ListOfInteractablePlane[i].pathTileReference.typeOfPathTile == PathTile.TypeOfPathTile.endTile) {
                    t_EndNode = new PathNode () {
                    absoluteIndex = i,
                    gridIndex = new Vector2 (
                    i / areaOfPuzzle,
                    i % areaOfPuzzle
                    ),
                    pathTileReference = m_ListOfInteractablePlane[i].pathTileReference
                    };
                } else if (m_ListOfInteractablePlane[i].pathTileReference.typeOfPathTile != PathTile.TypeOfPathTile.blockTile) {
                    t_ListOfRoadNode.Add (new PathNode () {
                        absoluteIndex = i,
                            gridIndex = new Vector2 (
                                i / areaOfPuzzle,
                                i % areaOfPuzzle
                            ),
                            pathTileReference = m_ListOfInteractablePlane[i].pathTileReference
                    });
                }
            }
        }
        t_ListOfRoadNode.Add (t_EndNode);

        PathNode t_RootNode = t_ListOfRoadNode[0];
        t_ListOfRoadNode.RemoveAt (0);

        m_Path = new List<Transform> ();
        m_Path.Add (t_RootNode.pathTileReference.GetTransformReferenceOfWorldPosition () [0]);

        while (t_RootNode != null) {

            bool t_FoundEndNode = false;
            int t_NumberOfAvailableNode = t_ListOfRoadNode.Count;
            
            for (int i = 0; i < t_NumberOfAvailableNode; i++) {

                Dictionary<int, float> t_DistanceMap = new Dictionary<int, float> ();
                Dictionary<int, int> t_DistanceNodeMap = new Dictionary<int, int> ();
                if (IsNodeAIsAdjacentOfNodeB (t_RootNode.gridIndex, t_ListOfRoadNode[i].gridIndex)) {

                    List<Vector3> t_PositionOnReferenceNode = t_RootNode.pathTileReference.GetWorldPosition ();
                    List<Vector3> t_PositionOnTargetNode = t_ListOfRoadNode[i].pathTileReference.GetWorldPosition ();

                    int t_NumberOfPositionOnReferenceNode = t_PositionOnReferenceNode.Count;
                    int t_NumberOfPositionOnTargetNode = t_PositionOnTargetNode.Count;

                    for (int j = 0; j < t_NumberOfPositionOnReferenceNode; j++) {

                        bool t_isForceQuit = false;
                        for (int k = 0; k < t_NumberOfPositionOnTargetNode; k++) {

                            if (Vector3.Distance (t_PositionOnReferenceNode[j], t_PositionOnTargetNode[k]) <= 0.1f) {

                                t_DistanceMap.Add (i, Vector3.Distance (t_PositionOnTargetNode[0], t_EndNode.pathTileReference.GetWorldPosition () [0]));
                                t_DistanceNodeMap.Add (i, k);
                                //Debug.Log ("Key (" + i + ", " + Vector3.Distance (t_PositionOnTargetNode[0], t_EndNode.pathTileReference.GetWorldPosition () [0]) + ") | KeyIndex(" + i + ", " + k + ")");

                                if (i == t_NumberOfAvailableNode - 1)
                                    t_FoundEndNode = true;

                                t_isForceQuit = true;
                                break;
                            }
                        }

                        if (t_isForceQuit)
                            break;
                    }

                }
                
                float t_MinimumDistance = t_DistanceMap.Values.Min ();
                int t_Key               = t_DistanceMap.FirstOrDefault (x => x.Value == t_MinimumDistance).Key;

                int t_KeyIndex = -1;
                t_DistanceNodeMap.TryGetValue (t_Key, out t_KeyIndex);
                //Debug.Log ("MinimumDistance = " + t_MinimumDistance + ", Key = " + t_Key + ", KeyIndex = " + t_KeyIndex);

                m_Path.Add (t_ListOfRoadNode[t_Key].pathTileReference.GetTransformReferenceOfWorldPosition () [t_KeyIndex]);
                m_Path.Add (t_ListOfRoadNode[t_Key].pathTileReference.GetTransformReferenceOfWorldPosition () [0]);

                t_RootNode = t_ListOfRoadNode[t_Key];
                t_ListOfRoadNode.RemoveAt (t_Key);
                t_NumberOfAvailableNode--;
                i--;

                if (t_ListOfRoadNode.Count == 0) {
                    t_RootNode = null;
                } 

                if (t_FoundEndNode)
                    break;
            }
        }

    }

    public void StoreResultedPathTile(){

        if(m_ResultedPathTile != null)
            m_ResultedPathTile = new List<PathTile>();
    
        m_ResultedPathTile.Clear();

        int t_NumberOfPathTile = m_ListOfInteractablePlane.Count;
        for(int i = 0; i < t_NumberOfPathTile; i++){

            m_ResultedPathTile.Add(m_ListOfInteractablePlane[i].pathTileReference);
        }
    }


#endif

    #endregion    
}