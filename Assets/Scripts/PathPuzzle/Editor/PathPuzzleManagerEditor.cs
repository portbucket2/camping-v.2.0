﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor (typeof (PathPuzzleManager))]
public class PathPuzzleManagerEditor : Editor {

    private PathPuzzleManager Reference;

    private ReorderableList m_ReorderListOfInteractablePlane;

    private SerializedProperty m_ColorOfActiveArea;
    private SerializedProperty m_ColorOfInactiveArea;
    private SerializedProperty m_GameRules;
    private SerializedProperty m_SizeForUnit;
    private SerializedProperty m_AreaOfPuzzle;
    private SerializedProperty m_ListOfInteractablePlane;
    private SerializedProperty m_LayerOfInteractableTile;

    private void OnEnable () {

        Reference = (PathPuzzleManager) target;

        m_GameRules = serializedObject.FindProperty ("gameRules");
        m_ColorOfActiveArea = serializedObject.FindProperty ("colorOfActiveArea");
        m_ColorOfInactiveArea = serializedObject.FindProperty ("colorOfInactiveArea");
        m_SizeForUnit = serializedObject.FindProperty ("sizeForUnit");
        m_AreaOfPuzzle = serializedObject.FindProperty ("areaOfPuzzle");
        m_ListOfInteractablePlane = serializedObject.FindProperty ("m_ListOfInteractablePlane");
        m_LayerOfInteractableTile = serializedObject.FindProperty ("layerOfInteractableTile");

        m_ReorderListOfInteractablePlane = new ReorderableList (serializedObject, m_ListOfInteractablePlane) {
            displayAdd = true,
                displayRemove = true,
                draggable = true,
                drawHeaderCallback = rect => {
                    EditorGUI.LabelField (new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), "InteractablePlane");
                },
                drawElementCallback = (rect, index, isActive, isFocused) => {

                    float t_HeightAdjustment = 0f;

                    SerializedProperty t_ListElement = m_ListOfInteractablePlane.GetArrayElementAtIndex (index);
                    SerializedProperty t_PathTileReference = t_ListElement.FindPropertyRelative ("pathTileReference");

                    if (Reference.m_ListOfInteractablePlane[index].pathTileReference != null) {

                        SerializedObject t_SerializedPathTile = new SerializedObject (Reference.m_ListOfInteractablePlane[index].pathTileReference);

                        t_SerializedPathTile.Update ();
                        SerializedProperty t_OnDraggingAllowed = t_SerializedPathTile.FindProperty ("onDraggingAllowed");
                        EditorGUI.PropertyField (
                            new Rect (rect.x, rect.y + t_HeightAdjustment, rect.width, EditorGUIUtility.singleLineHeight),
                            t_OnDraggingAllowed
                        );

                        t_HeightAdjustment += EditorGUIUtility.singleLineHeight;

                        if (!t_OnDraggingAllowed.boolValue) {

                            SerializedProperty t_TypeOfPathTile = t_SerializedPathTile.FindProperty ("typeOfPathTile");
                            EditorGUI.PropertyField (
                                new Rect (rect.x, rect.y + t_HeightAdjustment, rect.width, EditorGUIUtility.singleLineHeight),
                                t_TypeOfPathTile
                            );

                            t_HeightAdjustment += EditorGUIUtility.singleLineHeight;
                        }

                        t_SerializedPathTile.ApplyModifiedProperties ();
                    }

                    EditorGUI.BeginChangeCheck ();
                    EditorGUI.ObjectField (
                        new Rect (rect.x, rect.y + t_HeightAdjustment, rect.width, EditorGUIUtility.singleLineHeight),
                        t_PathTileReference);
                    if (EditorGUI.EndChangeCheck ()) {

                        serializedObject.FindProperty ("isOriginWillBeResetForPathTile").boolValue = true;
                    }

                    t_HeightAdjustment += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField (
                        new Rect (rect.x, rect.y + t_HeightAdjustment, rect.width, EditorGUIUtility.singleLineHeight),
                        "",
                        GUI.skin.horizontalSlider);
                },
                elementHeightCallback = index => {

                    float t_Height = EditorGUIUtility.singleLineHeight * 2;

                    if (Reference.m_ListOfInteractablePlane[index].pathTileReference != null) {

                        t_Height += EditorGUIUtility.singleLineHeight;

                        if (!Reference.m_ListOfInteractablePlane[index].pathTileReference.onDraggingAllowed) {

                            t_Height += EditorGUIUtility.singleLineHeight;
                        }
                    }

                    return t_Height;
                }
        };

        m_ReorderListOfInteractablePlane.onReorderCallback += OnReorderTheListCallback;
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        EditorGUILayout.BeginHorizontal (); {

            if (GUILayout.Button ("FindPath & StoreResult")) {

                Reference.GeneratePath ();
                Reference.StoreResultedPathTile();
                EditorUtility.SetDirty (Reference);
            }
        }
        EditorGUILayout.EndHorizontal ();

        EditorGUILayout.PropertyField (serializedObject.FindProperty ("m_Path"));
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("m_ResultedPathTile"));

        DrawHorizontalLine ();

        EditorGUILayout.PropertyField (m_GameRules);
        EditorGUILayout.PropertyField (m_ColorOfActiveArea);
        EditorGUILayout.PropertyField (m_ColorOfInactiveArea);

        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.PropertyField (m_SizeForUnit);
        if (EditorGUI.EndChangeCheck ()) {

            Reference.ResizeInteractablePlane ();
        }

        EditorGUI.BeginChangeCheck ();
        Reference.areaOfPuzzle = EditorGUILayout.IntSlider (
            "SizeOfArea",
            Reference.areaOfPuzzle,
            1,
            10
        );
        if (EditorGUI.EndChangeCheck ()) {
            Reference.InitializeInteractablePlane ();
        }

        EditorGUILayout.Space ();
        DrawHorizontalLine ();

        EditorGUILayout.PropertyField (m_LayerOfInteractableTile);

        EditorGUILayout.Space ();
        DrawHorizontalLine ();

        Reference.showInteractableList = EditorGUILayout.Foldout (
            Reference.showInteractableList,
            "InteractableTile",
            true
        );

        if (Reference.showInteractableList) {

            EditorGUI.indentLevel += 1;

            m_ReorderListOfInteractablePlane.DoLayoutList ();
            if (Reference.isOriginWillBeResetForPathTile) {

                Reference.RePositionInteractablePlane ();
                //Reference.StoreResultedPathTile();

                int t_NumberOfItemInList = Reference.m_ListOfInteractablePlane.Count;
                for (int i = 0; i < t_NumberOfItemInList; i++) {

                    if (Reference.m_ListOfInteractablePlane[i].pathTileReference != null) {

                        Reference.m_ListOfInteractablePlane[i].pathTileReference.transform.SetParent (Reference.m_ListOfInteractablePlane[i].interactablePlaneReference.transform);
                        Reference.m_ListOfInteractablePlane[i].pathTileReference.transform.localPosition = Vector3.zero;
                    }
                }
                Reference.isOriginWillBeResetForPathTile = false;

            }

            EditorGUI.indentLevel -= 1;
        }

        //DrawHorizontalLine();
        //DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties ();
    }

    void OnSceneGUI () {

    }

    #region Configuretion

    private void OnReorderTheListCallback (ReorderableList t_ReorderableList) {

        Reference.RePositionInteractablePlane ();
        //Reference.StoreResultedPathTile();
    }

    #endregion

    #region Editor Moduler Function

    private void DrawHorizontalLine () {

        EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
    }

    private void DrawSettingsEditor (Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

        if (settings != null) {

            using (var check = new EditorGUI.ChangeCheckScope ()) {

                foldout = EditorGUILayout.InspectorTitlebar (foldout, settings);

                if (foldout) {

                    CreateCachedEditor (settings, null, ref editor);
                    editor.OnInspectorGUI ();

                    if (check.changed) {

                        if (OnSettingsUpdated != null) {

                            OnSettingsUpdated.Invoke ();
                        }
                    }
                }
            }
        }
    }

    #endregion

}