﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (PathTile))]
public class PathTileEditor : Editor {
    private PathTile Reference;
    private SerializedProperty m_RadiusOfWaypoint;
    private SerializedProperty m_ColorOfRadius;
    private SerializedProperty m_Forward;
    private SerializedProperty m_Backward;
    private SerializedProperty m_Right;
    private SerializedProperty m_Left;

    private void OnEnable () {

        Reference = (PathTile) target;
        m_RadiusOfWaypoint = serializedObject.FindProperty ("radiusOfWaypoint");
        m_ColorOfRadius = serializedObject.FindProperty ("colorOfWaypoints");
        m_Forward = serializedObject.FindProperty ("forward");
        m_Backward = serializedObject.FindProperty ("backward");
        m_Right = serializedObject.FindProperty ("right");
        m_Left = serializedObject.FindProperty ("left");
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        m_RadiusOfWaypoint.floatValue = EditorGUILayout.Slider (
            "RadiusOfWaypoint",
            m_RadiusOfWaypoint.floatValue,
            0.1f,
            1f);
        m_ColorOfRadius.colorValue = EditorGUILayout.ColorField (
            "ColorOfWaypoint",
            m_ColorOfRadius.colorValue);

        DrawHorizontalLine ();

        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.BeginHorizontal (); {

            m_Forward.boolValue = EditorGUILayout.Toggle ("forward", m_Forward.boolValue);
            m_Backward.boolValue = EditorGUILayout.Toggle ("backward", m_Backward.boolValue);
        }
        EditorGUILayout.EndHorizontal ();

        EditorGUILayout.BeginHorizontal (); {
            m_Right.boolValue = EditorGUILayout.Toggle ("right", m_Right.boolValue);
            m_Left.boolValue = EditorGUILayout.Toggle ("left", m_Left.boolValue);
        }
        EditorGUILayout.EndHorizontal ();
        if (EditorGUI.EndChangeCheck ()) {

            serializedObject.FindProperty ("isCreatePositionNode").boolValue = true;
            
        }

        DrawHorizontalLine();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_ListOfPositionNodes"));

        if(Reference.isCreatePositionNode){

            Reference.InitializePositionNode();
        }

        serializedObject.ApplyModifiedProperties ();
    }

    #region Editor Moduler Function

    private void DrawHorizontalLine () {

        EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
    }

    private void DrawSettingsEditor (Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

        if (settings != null) {

            using (var check = new EditorGUI.ChangeCheckScope ()) {

                foldout = EditorGUILayout.InspectorTitlebar (foldout, settings);

                if (foldout) {

                    CreateCachedEditor (settings, null, ref editor);
                    editor.OnInspectorGUI ();

                    if (check.changed) {

                        if (OnSettingsUpdated != null) {

                            OnSettingsUpdated.Invoke ();
                        }
                    }
                }
            }
        }
    }

    #endregion

}