﻿using System.Collections.Generic;
using UnityEngine;
public class PathTile : MonoBehaviour {
    
    #region Custom Variables

    public enum TypeOfPathTile {
        blockTile,
        pathTile,
        startTile,
        endTile

    }

    #endregion

    #region Public Variables

    
#if UNITY_EDITOR

    public bool isCreatePositionNode = false;
    public float radiusOfWaypoint = 0.1f;
    public Color colorOfWaypoints = Color.magenta;

#endif

    public TypeOfPathTile typeOfPathTile = TypeOfPathTile.pathTile;
    public bool onDraggingAllowed;

    public bool forward;
    public bool backward;
    public bool right;
    public bool left;

    public List<Transform> m_ListOfPositionNodes;

    #endregion

    #region Mono Behaviour

    private void OnDrawGizmosSelected () {

        Gizmos.color = colorOfWaypoints;
        Gizmos.matrix = this.transform.localToWorldMatrix;

        if (forward)
            Gizmos.DrawWireSphere (
                Vector3.forward * 0.5f,
                radiusOfWaypoint);
        if (backward)
            Gizmos.DrawWireSphere (
                Vector3.back * 0.5f,
                radiusOfWaypoint);
        if (right)
            Gizmos.DrawWireSphere (
                Vector3.right * 0.5f,
                radiusOfWaypoint);
        if (left)
            Gizmos.DrawWireSphere (
                Vector3.left * 0.5f,
                radiusOfWaypoint);
    }

    #endregion

    #region Public Callback

    public List<Vector3> GetWorldPosition(){

        int t_NumberOfNodePosition = m_ListOfPositionNodes.Count;
        List<Vector3> t_NodePositions = new List<Vector3>();
        for(int i = 0 ; i < t_NumberOfNodePosition; i++){
            t_NodePositions.Add(m_ListOfPositionNodes[i].position);
        }

        return t_NodePositions;
    }

    public List<Transform> GetTransformReferenceOfWorldPosition(){

        int t_NumberOfNodePosition = m_ListOfPositionNodes.Count;
        List<Transform> t_NodePositions = new List<Transform>();
        for(int i = 0 ; i < t_NumberOfNodePosition; i++){
            t_NodePositions.Add(m_ListOfPositionNodes[i]);
        }

        return t_NodePositions;
    }

#if UNITY_EDITOR

    public void InitializePositionNode () {

        if (m_ListOfPositionNodes == null) {

            m_ListOfPositionNodes = new List<Transform> ();

        }

        Transform t_PositionNode = transform.Find ("PositionNode (Center)");
        if (t_PositionNode == null) {
            GameObject t_CenterPositionNode = new GameObject ();
            t_CenterPositionNode.name = "PositionNode (Center)";
            t_CenterPositionNode.transform.SetParent (transform);
            t_CenterPositionNode.transform.localPosition = Vector3.zero;
            m_ListOfPositionNodes.Add(t_CenterPositionNode.transform);
        }

        t_PositionNode = transform.Find ("PositionNode (Forward)");
        if (t_PositionNode == null) {
            if (forward) {
                GameObject t_NewPositionNode = new GameObject ();
                t_NewPositionNode.name = "PositionNode (Forward)";
                t_NewPositionNode.transform.SetParent (transform);
                t_NewPositionNode.transform.localPosition = Vector3.forward * 0.5f;
                m_ListOfPositionNodes.Add(t_NewPositionNode.transform);
            }
        } else if (!forward) {
            DestroyImmediate (t_PositionNode.gameObject);
        }

        t_PositionNode = transform.Find ("PositionNode (Backward)");
        if (t_PositionNode == null) {
            if (backward) {
                GameObject t_NewPositionNode = new GameObject ();
                t_NewPositionNode.name = "PositionNode (Backward)";
                t_NewPositionNode.transform.SetParent (transform);
                t_NewPositionNode.transform.localPosition = Vector3.back * 0.5f;
                m_ListOfPositionNodes.Add(t_NewPositionNode.transform);
            } 
        }else if (!backward) {
            DestroyImmediate (t_PositionNode.gameObject);
        }

        t_PositionNode = transform.Find ("PositionNode (Right)");
        if (t_PositionNode == null) {
            if (right) {

                GameObject t_NewPositionNode = new GameObject ();
                t_NewPositionNode.name = "PositionNode (Right)";
                t_NewPositionNode.transform.SetParent (transform);
                t_NewPositionNode.transform.localPosition = Vector3.right * 0.5f;
                m_ListOfPositionNodes.Add(t_NewPositionNode.transform);
            }
        }else if (!right) {
            DestroyImmediate (t_PositionNode.gameObject);
        }

        t_PositionNode = transform.Find ("PositionNode (Left)");
        if (t_PositionNode == null) {
            if (left) {

                GameObject t_NewPositionNode = new GameObject ();
                t_NewPositionNode.name = "PositionNode (Left)";
                t_NewPositionNode.transform.SetParent (transform);
                t_NewPositionNode.transform.localPosition = Vector3.left * 0.5f;
                m_ListOfPositionNodes.Add(t_NewPositionNode.transform);
            } 
        }else if (!left) {
            DestroyImmediate (t_PositionNode.gameObject);
        }

        isCreatePositionNode = false;
    }

#endif

    #endregion

}