﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePlaneBehaviour : MonoBehaviour
{

    #region Private Variables

    [SerializeField]
    private int m_IndexOfInteractableTile;

    #endregion

    #region Public Callback

    public void PreProcess(int t_IndexOfInteractableTile){

        m_IndexOfInteractableTile = t_IndexOfInteractableTile;
    }

    public int GetIndexOfInteractableTile(){

        return m_IndexOfInteractableTile;
    }

    #endregion
}
