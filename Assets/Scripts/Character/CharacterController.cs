﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CharacterController : MonoBehaviour
{

    #region Custom Variables

    public delegate void OnCharacterMovementStartEvent();
    public delegate void OnCharacterMovementEndEvent();

    public enum CharacterTag
    {
        player,
        enemy,
        static_objective,
        dynamic_objective
    }

    public enum PatrolDirection
    {
        rightToLeft,
        forwardToBackward,
        upToDown
    }


    #endregion

    #region Public Variables

    public OnCharacterMovementStartEvent    OnCharacterMovementStart;
    public OnCharacterMovementEndEvent      OnCharacterMovementEnd;

    public LayerMask m_DefaultLayerOfWall;
    [Range(0.1f,5f)]
    public float lengthOfRayCast;

    [Space(5.0f)]
    public CharacterTag characterTag;
    public PatrolDirection patrolDirection;

    [Space(5.0f)]
    [Range(0f,10f)]
    public float forwardVeloctiy;
    [Range(0f,1f)]
    public float angulerVeloity;

    [Space(5.0f)]
    public Animator animatorReference;
    public Rigidbody rigidbodyReferenceOfCharacterContainer;
    public Transform transformReferenceOfCharacterContainer;
    public Transform transfrmReferenceOfMeshContainer;

    #endregion

    #region Private Variables

    private Transform m_TransformReferenceOfCamera;
    private Vector3 m_CurrentMovingDirection;
    private LayerMask m_LayerOfWall;
    private bool m_IsGoingToPositiveDirection;
    private float m_SpeedMultiplier;
    

    #endregion

    #region Mono Behaviour

    protected void Awake(){

        m_TransformReferenceOfCamera = Camera.main.transform;

        StopCharcterMovement();

    }

    protected void Update(){

        RayCastOnWall();
        MoveCharacter();
        RotateCharacter(transfrmReferenceOfMeshContainer.position + (m_CurrentMovingDirection * lengthOfRayCast));
    }

    #endregion

    #region Configuretion

    private void RayCastOnWall(){

        switch(patrolDirection){

            case PatrolDirection.rightToLeft:
                if(m_IsGoingToPositiveDirection)
                    RayCast(
                        Vector3.right, 
                        m_LayerOfWall,
                        lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                else
                    RayCast(
                        Vector3.left,
                        m_LayerOfWall,
                        lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                break;
            case PatrolDirection.forwardToBackward:
                if(m_IsGoingToPositiveDirection)
                    RayCast(
                        Vector3.forward,
                        m_LayerOfWall,
                        lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                else
                    RayCast(
                        Vector3.back,
                        m_LayerOfWall,
                        lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                break;
            case PatrolDirection.upToDown:
                if(m_IsGoingToPositiveDirection)
                    RayCast(
                        Vector3.up,
                        m_LayerOfWall,
                        lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                else
                    RayCast(
                        Vector3.down,
                        m_LayerOfWall,
                        lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                break;
            default:
                RayCast(
                    Vector3.zero,
                    m_LayerOfWall,
                    lengthOfRayCast,
                        delegate{
                            FlipMovementDirection();
                        });
                break;
        }
    }

    protected void RayCast(
        Vector3 t_Direction, 
        LayerMask t_LayerMask,
        float t_LengthOfRayCast = 100f,
        UnityAction<RaycastHit> OnRayCastHit = null,
        UnityAction OnRayCastNoHit = null){

        Ray t_Ray = new Ray(transformReferenceOfCharacterContainer.position, t_Direction);
        
        #if UNITY_EDITOR

        Debug.DrawRay(
            transformReferenceOfCharacterContainer.position,
            t_Direction,
            Color.red
        );

        #endif

        RaycastHit t_RayCastHit;
        if(Physics.Raycast(t_Ray, out t_RayCastHit, t_LengthOfRayCast, t_LayerMask)){

            OnRayCastHit?.Invoke(t_RayCastHit);
            
        }else{

            OnRayCastNoHit?.Invoke();
        }
    }

    protected Vector3 GetMovingDirection(){

        switch(patrolDirection){

            case PatrolDirection.rightToLeft:
                if(m_IsGoingToPositiveDirection)
                    return Vector3.right;
                else
                    return Vector3.left;
            case PatrolDirection.forwardToBackward:
                if(m_IsGoingToPositiveDirection)
                    return Vector3.forward;
                else
                    return Vector3.back;
            case PatrolDirection.upToDown:
                if(m_IsGoingToPositiveDirection)
                    return Vector3.up;
                else
                    return Vector3.down;
            default:
                return Vector3.zero;
        }
    }

    private IEnumerator ControllerForLookingAtCamera(){

        float t_CycleLength = 0.033f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        while(!IsMonoBehaviourEnabled()){

            RotateCharacter(m_TransformReferenceOfCamera.position);
            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForLookingAtCamera());
    }

    private void MoveCharacter(){

        float t_Gravity = Mathf.Abs(rigidbodyReferenceOfCharacterContainer.velocity.y);
        float t_ModifiedGravity     = t_Gravity > forwardVeloctiy ? forwardVeloctiy : t_Gravity;
        float t_GravityValue        = 1f - ((t_ModifiedGravity / forwardVeloctiy));

        rigidbodyReferenceOfCharacterContainer.AddForce(
            m_CurrentMovingDirection * t_GravityValue * forwardVeloctiy * m_SpeedMultiplier,
            ForceMode.VelocityChange
        );
    }

    private void RotateCharacter(Vector3 t_LookDirection){

        Quaternion t_TargetedRotation = Quaternion.LookRotation(t_LookDirection - transfrmReferenceOfMeshContainer.position);
        t_TargetedRotation.x = 0f;
        t_TargetedRotation.z = 0f;
        Quaternion t_ModifiedRotation = Quaternion.Slerp(
            transfrmReferenceOfMeshContainer.rotation,
            t_TargetedRotation,
            angulerVeloity
        );

        transfrmReferenceOfMeshContainer.rotation = t_ModifiedRotation;
    }

    #endregion

    #region Public Callback

    public bool IsMonoBehaviourEnabled(){

        return enabled;
    }

    public void EnableMonoBehaviour(){

        enabled = true;
    }

    public void DisableMonoBehaviour(){

        enabled = false;
    }

    public void FlipMovementDirection(){

        m_IsGoingToPositiveDirection = !m_IsGoingToPositiveDirection;
        m_CurrentMovingDirection     = GetMovingDirection();
    }

    public void SetSpeedMultiplier(float t_SpeedMultiplier = 1f){

        m_SpeedMultiplier = t_SpeedMultiplier;
    }

    public void ShowAnimation(string t_AnimationKey){

        animatorReference.SetTrigger(t_AnimationKey);
    }

    public void StartCharacterMovement(
        float t_SpeedMultiplier = 1f,
        LayerMask t_LayerOfWall = default){

        m_SpeedMultiplier = t_SpeedMultiplier;

        if(t_LayerOfWall == default)
            m_LayerOfWall = m_DefaultLayerOfWall;
        else
            m_LayerOfWall = t_LayerOfWall;

        if(!IsMonoBehaviourEnabled()){

            ShowAnimation("WALK");

            m_IsGoingToPositiveDirection    = true;
            m_CurrentMovingDirection        = GetMovingDirection();
            EnableMonoBehaviour();
        
            OnCharacterMovementStart?.Invoke();
        }        
    }

    public void StopCharcterMovement(){

        OnCharacterMovementEnd?.Invoke();

        DisableMonoBehaviour();
        ShowAnimation("IDLE");
        StartCoroutine(ControllerForLookingAtCamera());
    }

    #endregion
}
