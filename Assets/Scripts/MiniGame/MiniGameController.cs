﻿using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Gameplay;

public class MiniGameController : MonoBehaviour
{

    #region Custom Variables

    protected delegate void OnMiniGameStartEvent();

    #endregion

    #region Public Variables

    public Camera       mainCameraReference;
    public LayerMask    layerForInteractingObject;
    

    #endregion

    #region Private Variables

    private enum TouchState
    {
        TouchDown,
        OnTouch,
        TouchUp
    }

    #endregion

    #region Protected Variables

    protected OnMiniGameStartEvent OnMiniGameStart;
    protected UnityAction<RaycastHit> OnInteractWithTheObjectOnTouchDown;
    protected UnityAction<RaycastHit> OnInteractWithTheObjectOnTouch;
    protected UnityAction<RaycastHit> OnInteractWithTheObjectOnTouchUp;

    #endregion

    #region  Private Variables

    protected UnityAction OnGameEnd;
    
    #endregion



    #region Mono Behaviour

    

    #endregion

    #region Configuretion

    private void OnTouchDown(Vector3 t_TouchPosition){

        OnRayCastFromCamera(t_TouchPosition, TouchState.TouchDown);
    }

    private void OnTouch(Vector3 t_TouchPosition){

        OnRayCastFromCamera(t_TouchPosition, TouchState.OnTouch);
    }

    private void OnTouchUp(Vector3 t_TouchPosition){

        OnRayCastFromCamera(t_TouchPosition, TouchState.TouchUp);
    }

    private void OnRayCastFromCamera(Vector3 t_TouchPosition, TouchState t_TouchState){

        Ray t_Ray = mainCameraReference.ScreenPointToRay(t_TouchPosition);
        RaycastHit t_RayCastHit;

        if(Physics.Raycast(t_Ray, out t_RayCastHit, 1000f,layerForInteractingObject)){
            
            switch(t_TouchState){
                case TouchState.TouchDown:
                    OnInteractWithTheObjectOnTouchDown?.Invoke(t_RayCastHit);
                break;

                case TouchState.OnTouch:
                    OnInteractWithTheObjectOnTouch?.Invoke(t_RayCastHit);
                break;

                case TouchState.TouchUp:
                    OnInteractWithTheObjectOnTouchUp?.Invoke(t_RayCastHit);
                break;
            }
                
        }
    }

    #endregion

    #region Public Callback

    public void EnableMonoBehaviour(){

        enabled = true;
    }

    public void DisableMonoBehaviour(){

        enabled = false;
    }


    public void StartGame(UnityAction OnGameEnd = null){

        this.OnGameEnd = OnGameEnd;

        GlobalTouchController.Instance.EnableTouchController();
        GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
        GlobalTouchController.Instance.OnTouch += OnTouch;
        GlobalTouchController.Instance.OnTouchUp += OnTouchUp;

        OnMiniGameStart?.Invoke();
    }

    public void EndGame(){

        OnGameEnd?.Invoke();
    }

    #endregion
}
