﻿using UnityEngine;

public class WaterParticleEvent : MonoBehaviour
{

    #region Public Variables

    public CoverCamperGameController coverCamperGameControllerReference;

    #endregion


    #region Mono Behaviour

    private void OnParticleCollision(GameObject other) {
        
        if(other.tag == "Tag_ProtectiveObject"){
            Debug.Log("Lowering HP");
            coverCamperGameControllerReference.LowerHP();
        }
    }

    #endregion
}
