﻿using UnityEngine;

using System.Collections;

using com.faithstudio.Math;
using com.faithstudio.ScriptedParticle;
using com.faithstudio.UI;


public class CoverCamperGameController : MiniGameController
{

    #region Public Variables

    [Header("Reference  :   External")]
    public ParticleController       particleForWaterSource;
    
    [Space(5.0f)]
    public LerpTouchController      playerMovementController;

    [Space(5.0f)]
    public LerpWaypointController   waterSourcePathController;
    public LerpWaypointController   protectiveObjectPathController;
    
    [Header("Configuretion  :   Gameplay")]
    [Range(1f,10f)]
    public float        defaultDurationForGame = 1;

    [Header("Configuretion  :   WaterSource")]
    [Range(1f,10f)]
    public float        defaultDamageOfWaterSource;
    

    [Header("Configuretion  :   ProtectiveObject")]
    public Transform    transfromReferenceOfProtectiveObject;
    public Vector3      offsetForHealthBar;
    [Range(0f,1000f)]
    public float        defaultHealthOfProtectiveObject = 100;
    [Range(0f,0.25f)]
    public float        randomDisplacement = 0.1f;
    

    #endregion

    #region Private Variables

    private UIProgressBar m_UIHealthBarReference;
    private Vector3 m_PreviousTouchPosition;

    private float m_RemainingTimeForGame;
    private float m_WaterDropDamage;
    private float m_MaxHealth;
    private float m_CurrentHealth;

    #endregion

    #region Mono Behaviour

    private void Awake () {

        base.OnMiniGameStart += PreProcess;
        
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForPreProcess(){


        float           t_CycleLength= 0.033f;
        WaitForSeconds  t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_TargetedLerpValueForWaterSource = Random.Range(0f,1f);
        float t_TargetedLerpValueForProtectiveObject = Mathf.Clamp01(t_TargetedLerpValueForWaterSource + Random.Range(-0.1f,0.1f));

        m_RemainingTimeForGame = defaultDurationForGame;

        UIProgressBar t_UIProgressBarReference =  UIStateController.Instnace.UIProgressbarReference;
        t_UIProgressBarReference.SetInitialValue(0);
        t_UIProgressBarReference.ShowProgressbar();

        particleForWaterSource.PlayParticle();

        while(m_RemainingTimeForGame > 0){

            float t_Progression = 1f - (m_RemainingTimeForGame / defaultDurationForGame);
            t_UIProgressBarReference.UpdateProgressBar(t_Progression);

            waterSourcePathController.LerpThroughPositions(t_TargetedLerpValueForWaterSource);

            if(Mathf.Abs(t_TargetedLerpValueForWaterSource - waterSourcePathController.GetCurrentLerpedValue()) <= 0.01f){

                t_TargetedLerpValueForWaterSource = Random.Range(0f,1f);
                t_TargetedLerpValueForProtectiveObject = Mathf.Clamp01(t_TargetedLerpValueForWaterSource + Random.Range(-randomDisplacement,randomDisplacement));

            }else{

                waterSourcePathController.LerpThroughPositions(t_TargetedLerpValueForWaterSource);
                protectiveObjectPathController.LerpThroughPositions(t_TargetedLerpValueForProtectiveObject);
            }            

            yield return t_CycleDelay;
            m_RemainingTimeForGame -= t_CycleLength;
        }


        t_UIProgressBarReference.HideProgressbar();
        m_UIHealthBarReference.HideProgressbar();
        PostProcess(m_CurrentHealth <= 0 ? false : true);

        StopCoroutine(ControllerForPreProcess());
    }

    private void PreProcess(){

        m_UIHealthBarReference= UIStateController.Instnace.UIFillBarReference;
        m_UIHealthBarReference.SetInitialValue(1f);

        m_WaterDropDamage   = defaultDamageOfWaterSource;
        m_MaxHealth         = defaultHealthOfProtectiveObject;
        m_CurrentHealth     = m_MaxHealth;

        playerMovementController.EnableLerpTouchController();
        waterSourcePathController.EnableMovement();

        StartCoroutine(ControllerForPreProcess());

    }

    private void PostProcess(bool t_IsPlayerWon){

        particleForWaterSource.StopParticle(false);
        playerMovementController.DisableLerpTouchController();
        waterSourcePathController.DisableMovement();

        base.EndGame();
    }

    #endregion

    #region Public Callback

    public void LowerHP(){

        m_CurrentHealth -= m_WaterDropDamage;
        float t_Progression = m_CurrentHealth / m_MaxHealth;

        m_UIHealthBarReference.ShowProgressbar();
        m_UIHealthBarReference.UpdateProgressBarWithFollowingObject(
            transfromReferenceOfProtectiveObject,
            offsetForHealthBar,
            t_Progression,
            delegate{
                m_UIHealthBarReference.HideProgressbar();
                if(m_CurrentHealth <= 0)
                    m_RemainingTimeForGame = 0;
            }

        );
    }

    #endregion
}
