﻿using UnityEngine;
using UnityEngine.Events;

public class MovingPinController : MonoBehaviour
{
    #region Public Variables

    public Transform transformReferenceOfPinContainer;
    [Range(0f,1f)]
    public float        forwardVelocityForMovingPin;

    [Space(5.0f)]
    public Transform[] pinPositions;

    #endregion

    #region Private Variables

    private bool    m_IsMovingPinToNextPosition = true;
    private int     m_CurrentIndexForPinPosition = 0;
    
    private UnityAction OnPinReachedDestination;

    #endregion

    #region Mono Behaviour

    private void Awake(){

       enabled = false; 
    }

    private void Update(){

        Vector3 t_PinPosition = pinPositions[m_CurrentIndexForPinPosition].position;
        Vector3 t_ModifiedPosition = Vector3.Lerp(
            transformReferenceOfPinContainer.position,
            t_PinPosition,
            forwardVelocityForMovingPin
        );
        
        if(Vector3.Distance(t_ModifiedPosition,t_PinPosition) <= 0.1f){

            transformReferenceOfPinContainer.position = t_PinPosition;
            OnPinReachedDestination?.Invoke();
            enabled = false;
        }

        transformReferenceOfPinContainer.position = t_ModifiedPosition;
    }

    #endregion

    #region Configuretion

    private bool IsValidIndexForPinPosition(int t_PinIndex){

        if(t_PinIndex >= 0 && t_PinIndex < pinPositions.Length)
            return true;

        return false;
    }

    private void SelectNextPositionForPin(){

        int t_NewIndexForPinPosition = m_CurrentIndexForPinPosition + (m_IsMovingPinToNextPosition ? 1 : -1);
        if(IsValidIndexForPinPosition(t_NewIndexForPinPosition)){

            if(t_NewIndexForPinPosition == 0 || t_NewIndexForPinPosition == pinPositions.Length - 1)
                m_IsMovingPinToNextPosition = !m_IsMovingPinToNextPosition;

            m_CurrentIndexForPinPosition = t_NewIndexForPinPosition;
        }
    }

    #endregion

    #region Public Callback

    /// <summary>
    /// 
    /// </summary>
    /// <param name="OnPinReachedDestination">Callback when pin reached destination</param>
    /// <returns>if 'true', pin movement command is taken, else, ignored as it is waiting for the previous movement to complete</returns>
    public bool MovePin(UnityAction OnPinReachedDestination = null){

        if(!enabled){

            this.OnPinReachedDestination = OnPinReachedDestination;
            SelectNextPositionForPin();
            enabled = true;

            return true;
        }
        

        return false;
    }

    #endregion
}
