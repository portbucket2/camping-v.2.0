﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingPinController))]
public class MovingPinControllerEditor : Editor
{
    private MovingPinController Reference;
    
    private void OnEnable() {
        
        if(target == null)
            return;
        
        Reference = (MovingPinController) target;
    }

    public override void OnInspectorGUI(){

        serializedObject.Update();

        if(Application.isPlaying){

            if(GUILayout.Button("Move Pin")){
                Reference.MovePin();
            }
            DrawHorizontalLine();
        }

        

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    #region Editor Moduler Function

    private void DrawHorizontalLine () {

        EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
    }

    private void DrawSettingsEditor (Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

        if (settings != null) {

            using (var check = new EditorGUI.ChangeCheckScope ()) {

                foldout = EditorGUILayout.InspectorTitlebar (foldout, settings);

                if (foldout) {

                    CreateCachedEditor (settings, null, ref editor);
                    editor.OnInspectorGUI ();

                    if (check.changed) {

                        if (OnSettingsUpdated != null) {

                            OnSettingsUpdated.Invoke ();
                        }
                    }
                }
            }
        }
    }

    #endregion

}
