﻿using UnityEngine;
using UnityEngine.Events;
public class PlayerControllerOfCollectableObjectHunter : CharacterController
{
    #region Public Variables

    public UnityAction OnCollidedWithEnemy;

    #endregion

    #region Private Variables

    #endregion

    #region Mono Behaviour

    private void OnCollisionEnter(Collision other) {
        
        if(other.collider.GetComponent<PlayerControllerOfEnemyOfCollectableObjectHunter>()){

            OnCollidedWithEnemy?.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other) {
        
        CollectableObjectBehaviour t_CollectableObject = other.GetComponent<CollectableObjectBehaviour>();
        if(t_CollectableObject != null){

            t_CollectableObject.ShowCollectingEffect();
        }
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void PreProcess(UnityAction OnCollidedWithEnemy){

        this.OnCollidedWithEnemy = OnCollidedWithEnemy;
    }

    #endregion
}
