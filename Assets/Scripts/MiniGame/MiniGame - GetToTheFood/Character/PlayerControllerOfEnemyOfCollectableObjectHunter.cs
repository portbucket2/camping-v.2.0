﻿using UnityEngine;
using UnityEngine.Events;

public class PlayerControllerOfEnemyOfCollectableObjectHunter : CharacterController {
    #region Public Variables

    [Header("Parameter  :   EnemyBehaviour")]
    [Range(1,25)]
    public float lengthOfDetectionRay = 1;
    public LayerMask detectionLayer;
    [Range(1,2)]
    public float speedMultiplierOnDetectingEnemy = 1;

    #endregion

    #region Private Variables

    private bool m_IsFoundPlayerOnMovementDirection;
    private bool m_IsFoundPlayerOnOppositeMovementDirection;
    private bool m_IsEnemySetToAggresive;

    private UnityAction OnCollidedWithPlayer;

    #endregion

    #region Mono Behaviour

    private new void Awake () {

        base.Awake();
    }

    private new void Update () {

        base.Update();

        Vector3 t_MovementDirection = base.GetMovingDirection ();
        Vector3 t_OppositeMovementDiretion = t_MovementDirection * -1f;

        base.RayCast (
            t_MovementDirection,
            detectionLayer,
            lengthOfDetectionRay,
            OnRayCastHitOnForwardDirection,
            delegate {

                m_IsFoundPlayerOnMovementDirection = false;
            });

        base.RayCast (
            t_OppositeMovementDiretion,
            detectionLayer,
            lengthOfDetectionRay,
            OnRayCastHitOnBackwardDirection,
            delegate {

                m_IsFoundPlayerOnOppositeMovementDirection = false;
            });

        if(m_IsFoundPlayerOnMovementDirection || m_IsFoundPlayerOnOppositeMovementDirection){

            if(!m_IsEnemySetToAggresive){

                if(m_IsFoundPlayerOnOppositeMovementDirection)
                    base.FlipMovementDirection();

                base.ShowAnimation("RUN");
                base.SetSpeedMultiplier(speedMultiplierOnDetectingEnemy);

                m_IsEnemySetToAggresive = true;
            }
        }else{

            if(m_IsEnemySetToAggresive){
                base.SetSpeedMultiplier(1f);
                m_IsEnemySetToAggresive = false;
            }
            
        }
    }

    #endregion

    #region Configuretion

    private void OnRayCastHitOnForwardDirection(RaycastHit t_RayCastHit){
        
        if(t_RayCastHit.collider.GetComponent<PlayerControllerOfCollectableObjectHunter>()){

            m_IsFoundPlayerOnMovementDirection = true;
        }
    }

    private void OnRayCastHitOnBackwardDirection(RaycastHit t_RayCastHit){
        
        if(t_RayCastHit.collider.GetComponent<PlayerControllerOfCollectableObjectHunter>()){

            m_IsFoundPlayerOnOppositeMovementDirection = true;
        }
    }

    #endregion

    #region Public Callback

    

    #endregion
}