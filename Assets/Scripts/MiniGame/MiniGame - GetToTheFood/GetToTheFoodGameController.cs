﻿using UnityEngine;
using System.Collections;
public class GetToTheFoodGameController : MiniGameController
{
    #region Public Variables

    public PlayerControllerOfCollectableObjectHunter          playerControllerReference;
    public CharacterController[]        enemyControllerReferences;
    public CollectableObjectBehaviour[] collectableObject;
    public MovingPinController[] movingPinReferences;
    


    #endregion

    #region Private Variables

    private int m_NumberOfCollectableObject;
    private int m_RemainingNumberOfCollectableObject;

    #endregion

    #region Mono Behaviour

    private void Awake(){

        m_NumberOfCollectableObject = collectableObject.Length;

        base.OnMiniGameStart += PreProcess;
        base.OnInteractWithTheObjectOnTouchDown += RayCastOnMovingPin;
    }

    #endregion

    #region Configuretion

    private void RayCastOnMovingPin(RaycastHit t_RayCastHit){

        MovingPinController t_MovingPinControllerReference = t_RayCastHit.collider.GetComponent<MovingPinController>();
        if(t_MovingPinControllerReference != null){

            t_MovingPinControllerReference.MovePin();
        }
    }

    private IEnumerator PostProcess(){

        yield return new WaitForSeconds(1f);

        foreach(CharacterController t_Character in enemyControllerReferences){
            t_Character.StopCharcterMovement();
        }

        yield return new WaitForSeconds(2f);
        
        base.EndGame();

        StopCoroutine(PostProcess());
    }

    #endregion

    #region Public Callback

    public void PreProcess(){

        m_RemainingNumberOfCollectableObject = m_NumberOfCollectableObject;

        playerControllerReference.PreProcess(base.EndGame);
        playerControllerReference.StartCharacterMovement();

        foreach(CharacterController t_Character in enemyControllerReferences){
            
            t_Character.StartCharacterMovement();
        }

        foreach(CollectableObjectBehaviour t_CollectableObject in collectableObject){

            t_CollectableObject.PreProcess(AcknowledgementOfCollectingTheObject);
        }
    }

    public void AcknowledgementOfCollectingTheObject(){

        m_RemainingNumberOfCollectableObject--;
        if(m_RemainingNumberOfCollectableObject <= 0){
            
            StartCoroutine(PostProcess());
        }
    }

    #endregion
}
